#pragma once

#include "GlobalResource.h"
#include <initializer_list>
#include <vector>
#include <string>
#include <GL/glew.h>
#include <cstdarg>

namespace Boop
{

	class ShaderManager : public GlobalResource<ShaderManager>
	{
	public:

		ShaderManager();

		~ShaderManager();

		GLuint add(std::string shader_name, std::initializer_list<std::string> list);

		GLuint at(std::string shader_name);

		void remove(std::string shader_name);

		bool exists(std::string shader_name);

	private:

		struct ShaderElement
		{
			std::string name;
			GLuint program;
		};

		GLuint get_shader_type(std::string s);

		std::vector<ShaderElement> shader_programs;


	};

}