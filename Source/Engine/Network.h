#pragma once

#include <WinSock2.h>
#include "GlobalResource.h"
#include "RakPeerInterface.h"
#include "FileListTransfer.h"
#include <string>
#include "Event.h"
#include "EventID.h"
#include <filesystem>
#include <IncrementalReadInterface.h>
#include "FileTransfer.h"
#include <vector>

namespace Boop
{
	/// Use to setup basic client/server system.
	/// Supports file transfer.
	class Network : public GlobalResource<Network>
	{
	public:

		/// @param thread_num The thread to run the network listener on.
		Network(int thread_num);
		~Network();

		/// Starts a client instance.
		/// @param host The remote host to connect to.
		void startClient(std::string host);

		/// Starts a server instance.
		/// @maxConnections Max simultaneous connections.
		void startServer(int maxConnections);

		/// Enables file transfer capability
		void enableFileTransfer();

		/// @returns True if connected on client. Always True if server.
		bool isConnected();

		/// @retuns True if is machine is acting as a server.
		bool isServer();

		enum FileTransferStatus
		{
			IN_PROGRESS,
			COMPLETED,
			FAILED
		};

		/// Sends a list of files to the specified address.
		/// @param statusOut This variable will be updated with the transfer status.
		/// @param address The address to send to.
		/// @param list The list of files to send.
		void sendFiles(FileTransferStatus* statusOut, const RakNet::SystemAddress& address, const std::vector<std::filesystem::path>& list);

		struct TransferRequest
		{
			uint16_t reqID;
			uint16_t setID;
			RakNet::SystemAddress systemAddress;
			RakNet::FileList fileList;
			FileTransferStatus* status;
		};

		/// Gets file request list.
		std::vector<TransferRequest>& getRequests();

		/// Sets the location to download files too when receiving files.
		void setDownloadLocation(const char* path);

		/// @returns The file download location.
		const std::filesystem::path& getDownloadLocation();

		/// @returns the RakNet peer object.
		RakNet::RakPeerInterface* getPeer();

		/// @returns The connection address at the specified index.
		RakNet::SystemAddress getConnectionAddress(int i = 0);

		enum Messages
		{
			ID_FILE_SEND_REQUEST = ID_USER_PACKET_ENUM + 1,
			ID_FILE_SEND_ACCEPTED,
			ID_FILE_SEND_DENIED,
			ID_FILE_REQUEST,
			ID_FILE_REQUEST_ACCEPTED,
			ID_FILE_REQUEST_DENIED,
			ID_FILE_DOWNLOAD_COMPLETE_NOTIFY,
			MAX
		};

		static const char FILE_ORDER_CHANNEL = 8;

	private:

		unsigned char getPacketIdentifier(RakNet::Packet* p);

		void update();

		uint16_t requestIDCounter;

		std::vector<TransferRequest> requests;
		RakNet::RakPeerInterface* peer;

		RakNet::FileListTransfer fileTransfer;
		RakNet::IncrementalReadInterface incrementalReadInterface;
		FileDownloadProgress fileDownloadProgress;
		FileUploadProgress fileUploadProgress;

		std::filesystem::path downloadLocation;
		Event frameUpdate;
		bool fIsConnected;
		bool fIsServer;
		bool fFileTransferEnabled;
		int thread;
	};

}