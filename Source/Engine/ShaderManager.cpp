
#include "ShaderManager.h"
#include "Logger.h"
#include <fstream>
#include <streambuf>
#include <assert.h>

namespace Boop
{

	ShaderManager::ShaderManager()
	{
	}

	ShaderManager::~ShaderManager()
	{
		for (auto it = shader_programs.begin(); it != shader_programs.end(); ++it)
		{
			glDeleteProgram(it->program);
		}
	}

	GLuint ShaderManager::add(std::string shader_name, std::initializer_list<std::string> list)
	{
		GLuint p = at(shader_name);
		if (p != -1)
			return p;

		ShaderElement element;
		element.name = shader_name;

		std::vector<GLuint> shaders;
		bool error_flag = false;
		for (const std::string* it = std::begin(list); it != std::end(list); ++it)
		{
			GLuint type = get_shader_type(*it);

			if (type != 0)
			{
				GLuint shader = glCreateShader(type);
				shaders.push_back(shader);
				std::ifstream file(it->c_str());
				assert(file);
				std::string source((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
				const char* c_source = source.c_str();
				glShaderSource(shader, 1, &c_source, 0);

				glCompileShader(shader);

				GLint is_compiled = 0;
				glGetShaderiv(shader, GL_COMPILE_STATUS, &is_compiled);
				if (is_compiled == GL_FALSE)
				{
					GLint max_len = 0;
					glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &max_len);

					// The maxLength includes the NULL character
					GLchar* error_log = new GLchar[max_len];
					glGetShaderInfoLog(shader, max_len, &max_len, &error_log[0]);
					Logger::out() << "Shader compilation error" << shader_name.c_str() << ":" << (*it).c_str() << "\n" << error_log << std::endl;
					delete[] error_log;

					error_flag = true;
				}
			}
		}

		if (!error_flag)
		{
			element.program = glCreateProgram();

			for (int i = 0; i < shaders.size(); ++i)
			{
				glAttachShader(element.program, shaders[i]);
			}

			glLinkProgram(element.program);

			GLint isLinked = 0;
			glGetProgramiv(element.program, GL_LINK_STATUS, (int*)&isLinked);
			if (isLinked == GL_FALSE)
			{
				GLint max_len = 0;
				glGetProgramiv(element.program, GL_INFO_LOG_LENGTH, &max_len);

				// The maxLength includes the NULL character
				GLchar* error_log = new GLchar[max_len];
				glGetProgramInfoLog(element.program, max_len, &max_len, &error_log[0]);
				Logger::out() << "Program linking error " << shader_name.c_str() << "\n" << error_log << std::endl;
				delete[] error_log;

				// We don't need the program anymore.
				glDeleteProgram(element.program);

				error_flag = true;
			}
		}

		for (int i = 0; i < shaders.size(); ++i)
		{
			if (error_flag)
				glDeleteShader(shaders[i]);
			else
				glDetachShader(element.program, shaders[i]);
		}

		if (!error_flag)
		{
			shader_programs.push_back(element);
			return element.program;
		}

		assert(false);
		return -1;
	}

	GLuint ShaderManager::at(std::string shader_name)
	{
		for (auto it = shader_programs.begin(); it != shader_programs.end(); ++it)
		{
			if (it->name == shader_name)
			{
				return it->program;
			}
		}
		return -1;
	}

	void ShaderManager::remove(std::string shader_name)
	{
		for (auto it = shader_programs.begin(); it != shader_programs.end(); ++it)
		{
			if (it->name == shader_name)
			{
				shader_programs.erase(it);
			}
		}
	}

	bool ShaderManager::exists(std::string shader_name)
	{
		for (auto it = shader_programs.begin(); it != shader_programs.end(); ++it)
		{
			if (it->name == shader_name)
			{
				return true;
			}
		}
		return false;
	}

	GLuint ShaderManager::get_shader_type(std::string s)
	{
		size_t res;

		res = s.find(".vs");
		if (res != std::string::npos)
		{
			return GL_VERTEX_SHADER;
		}

		res = s.find(".fs");
		if (res != std::string::npos)
		{
			return GL_FRAGMENT_SHADER;
		}

		res = s.find(".gs");
		if (res != std::string::npos)
		{
			return GL_GEOMETRY_SHADER;
		}

		res = s.find(".tc");
		if (res != std::string::npos)
		{
			return GL_TESS_CONTROL_SHADER;
		}

		res = s.find(".te");
		if (res != std::string::npos)
		{
			return GL_TESS_EVALUATION_SHADER;
		}

		return 0;
	}

}