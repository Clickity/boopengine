#pragma once

#include <functional>
#include <deque>
#include <mutex>
#include <memory>
#include <condition_variable>
#include <stdint.h>

namespace Boop
{

	class WorkerThread
	{
	public:
		struct RunInstance
		{
			std::function<void(void*)> func;
			std::shared_ptr<void> data;
			uint64_t uid;
			bool* finished;
		};

		WorkerThread();
		~WorkerThread();

		/// Call to stop the thread.
		void stop();

		/// Schedules a function to run on this thread.
		/// @param func The funciton to run.
		/// @param data Shared pointer to data to send to the function. NULL is valid if no data is necessary.
		/// @param finished_flag Pointer to a flag that is set to true when the function has finishe execution.
		/// @param uid UID used for removal from queue if that is necessary.
		void run_this(std::function<void(void*)> func, std::shared_ptr<void> data = 0, bool* finished_flag = 0, uint64_t uid = 0);

		/// @param uid Removes the specified uid from the run queue.
		void remove(uint64_t uid);

		/// @returns The number of functions scheduled in the run queue.
		int num_runners();

		/// @returns The internal thread id identifier.
		std::thread::id get_thread_id();

	private:

		void start();

		std::deque<RunInstance> runners;
		std::mutex runner_mtx;
		std::thread thd;
		std::condition_variable sleeper;
		std::thread::id thread_id;
		uint64_t running_uid;
		bool is_running;
		bool exit_success;
		int thread_n;

		static int thread_cnt;
	};

}