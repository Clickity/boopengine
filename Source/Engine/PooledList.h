#pragma once

#include "Pool.h"


/// A linked list that uses a pool allocator.
template<class T>
class PooledList
{
public:

	class Node
	{
	public:
		T val;
		Node* next;
		Node* prev;
	};

	/// Different from C++ iterators in that `it.end()` is how you
	/// determine the end.
	class Iterator 
	{
	public:
		Iterator& operator++();
		Iterator operator++(int);
		T& operator*();
		T* operator->();
		bool end();

		Node* node;
	};

	PooledList();
	PooledList(PoolBase<Node>*);
	void setPool(PoolBase<T>* p);
	void push_back(const T& val);
	void push_front(const T& val);
	T& front();
	T& back();
	void remove(T* v);
	void remove(Iterator& it);
	int size();
	Iterator begin();

private:
	Node* head;
	Node* tail;
	PoolBase<Node>* pool;
	int num;
};

template<class T>
PooledList<T>::PooledList()
	: pool(0)
	, head(0)
	, tail(0)
	, num(0)
{

}

template<class T>
PooledList<T>::PooledList(PoolBase<Node>* p)
	: PooledList()
{
	pool = p;
}

template<class T>
void PooledList<T>::setPool(PoolBase<T>* p)
{
	pool = p;
}

template<class T>
void PooledList<T>::push_back(const T& val)
{
	Node node;
	node.val = val;
	node.next = 0;
	node.prev = tail;
	tail = pool->insert(node);
	if (head == 0) head = tail;
	if (node.prev)
	{
		node.prev->next = tail;
	}
	++num;
}

template<class T>
void PooledList<T>::push_front(const T& val)
{
	Node node;
	node.val = val;
	node.next = head;
	node.prev = 0;
	head = pool->insert(node);
	if(tail == 0) tail = head;
	if (node.next)
	{
		node.next->prev = head;
	}
	++num;
}

template<class T>
T& PooledList<T>::front()
{
	return head->val;
}

template<class T>
T& PooledList<T>::back()
{
	return tail->val;
}

template<class T>
void PooledList<T>::remove(T* v)
{
	Node* node = reinterpret_cast<Node*>(v);
	if (node->prev)
	{
		node->prev->next = node->next;
	}
	else {
		head = node->next;
	}
	if (node->next)
	{
		node->next->prev = node->prev;
	}
	else {
		tail = node->prev;
	}
	pool->remove(node);
	--num;
}

template<class T>
void PooledList<T>::remove(Iterator& it)
{
	remove(&(*it));
}

template<class T>
int PooledList<T>::size()
{
	return num;
}

template<class T>
typename PooledList<T>::Iterator PooledList<T>::begin()
{
	Iterator it;
	it.node = head;
	return it;
}

template<class T>
typename PooledList<T>::Iterator& PooledList<T>::Iterator::operator++()
{
	node = node->next;
	return *this;
}

template<class T>
typename PooledList<T>::Iterator PooledList<T>::Iterator::operator++(int)
{
	Iterator it = *this;
	operator++();
	return it;
}

template<class T>
T& PooledList<T>::Iterator::operator*()
{
	return node->val;
}

template<class T>
T* PooledList<T>::Iterator::operator->()
{
	return &node->val;
}

template<class T>
bool PooledList<T>::Iterator::end()
{
	return node == 0;
}

