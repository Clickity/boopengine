#include "Application.h"

#include "Logger.h"
#include "Scheduler.h"
#include "EventDispatcher.h"
#include "Event.h"
#include "EventID.h"
#include <GL/glew.h>
#include "Utility.h"

namespace Boop
{

    Application::Application(const ApplicationSettings& settings_i) :
        periodic_event(0),
        window(0),
        gl_context(0),
        scheduled_thread(THIS_THREAD)
    {
        settings = settings_i;
        EventDispatcher::get()->run_this(settings.thread_num, std::bind(&Application::deferred_init, this, _1));
        buildFastCosineTable();
    }

    Application::~Application()
    {
        if (periodic_event)
            delete periodic_event;

        SDL_DestroyWindow(window);

        SDL_Quit();

        Logger::out() << "Application Destroyed" << std::endl;;
    }

    void Application::deferred_init(void*)
    {

        SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);

        int res = 0;
        res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        res += SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        res += SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        res += SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
        res += SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        res += SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        res += SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        res += SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        res += SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        //res += SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        //res += SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
        assert(res == 0);

        uint32_t flags = SDL_WINDOW_OPENGL;
        if (settings.is_fullscreen)
            if (settings.is_default_resolution)
                flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
            else
                flags |= SDL_WINDOW_FULLSCREEN;

        if (settings.is_maximized)
            flags |= SDL_WINDOW_MAXIMIZED;

        if (settings.is_resizable)
            flags |= SDL_WINDOW_RESIZABLE;


        window = SDL_CreateWindow(
            settings.application_name.c_str(),                    // window title
            settings.window_offset_x,						      // initial x position
            settings.window_offset_y,						      // initial y position
            settings.window_width,                                // width, in pixels
            settings.window_height,                               // height, in pixels
            flags											      // flags - see below
        );

        gl_context = SDL_GL_CreateContext(window);

        ThreadPool* tp = EventDispatcher::get()->get_thread_pool();
        if (tp != 0)
        {
            int this_thread = tp->get_this_thread();
            if (this_thread != NO_THREAD)
            {
                scheduled_thread = this_thread;
            }
        }

        int w, h;
        SDL_GetWindowSize(window, &w, &h);
        SDL_GL_SetSwapInterval(0);

        glViewport(0, 0, w, h);
        glClearColor(0.f, 0.f, 0.f, 1.0f);
        //glEnable(GL_MULTISAMPLE);
        glEnable(GL_CULL_FACE);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        SDL_GL_SwapWindow(get_window()); // The early swap is to make the screen black during loading.

        periodic_event = new Event(SystemEventID::FRAME_UPDATE, Channel::SYSTEM, scheduled_thread, std::bind(&Application::event_handler, this));
        periodic_event->repeat();

        set_initialized(true);
        Logger::out() << "Application Initialized" << std::endl;;
    }

    SDL_Window* Application::get_window()
    {
        return window;
    }

    SDL_GLContext Application::get_gl_context()
    {
        return gl_context;
    }

    int Application::get_width()
    {
        int w;
        SDL_GetWindowSize(window, &w, 0);
        return w;
    }

    int Application::get_height()
    {
        int h;
        SDL_GetWindowSize(window, 0, &h);
        return h;
    }

    int Application::get_mouse_x()
    {
        int v;
        SDL_GetMouseState(&v, 0);
        return v;
    }

    int Application::get_mouse_y()
    {
        int v;
        SDL_GetMouseState(0, &v);
        return v;
    }

    bool Application::isKeyDown(SDL_Scancode key)
    {
        const uint8_t* state = SDL_GetKeyboardState(0);
        return state[key];
    }

    void Application::event_handler()
    {
        SDL_Event event;
        EventDispatcher* ed = EventDispatcher::get();
        Scheduler* sch = Scheduler::get();
        while (SDL_PollEvent(&event) && sch->is_running())
        {
            if (event.type == SDL_QUIT)
            {
                sch->stop();
            }
            else {
                ed->dispatch(event.type, Channel::SDL, std::make_shared<SDL_Event>(event));
            }
        }
    }

}