#include "Texture.h"
#include <algorithm>
#include <assert.h>
#include <stdint.h>

namespace Boop
{

	Texture::Texture(const char* filename)
	{
		tbo = load_file(filename);
	}

	Texture::Texture(float r, float g, float b, float a)
	{
		glGenTextures(1, &tbo);
		glBindTexture(GL_TEXTURE_2D, tbo);

		uint8_t color[4];
		color[0] = (uint8_t)(r * 255.0);
		color[1] = (uint8_t)(g * 255.0);
		color[2] = (uint8_t)(b * 255.0);
		color[3] = (uint8_t)(a * 255.0);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, color);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		assert(glGetError() == GL_NO_ERROR);
	}

	Texture::~Texture()
	{
		glDeleteTextures(1, &tbo);
	}

	GLuint Texture::get_buffer()
	{
		return tbo;
	}

	GLuint Texture::load_file(const char* filename, int* out_width, int* out_height)
	{
		GLuint tbo = 0;
		SDL_Surface* surface = IMG_Load(filename);

		typedef uint8_t byte_type;

		const unsigned bytesPerRow = surface->pitch;
		const byte_type* originalRaw = static_cast<byte_type*>(surface->pixels);
		byte_type* flippedRaw = new byte_type[bytesPerRow * surface->h];

		for (int i = 0; i < surface->h; ++i)
		{
			const byte_type* srcBeg = originalRaw + (bytesPerRow * (surface->h - i - 1));
			const byte_type* srcEnd = srcBeg + bytesPerRow;

			std::copy(srcBeg, srcEnd, flippedRaw + bytesPerRow * i);
		}

		glGenTextures(1, &tbo);
		glBindTexture(GL_TEXTURE_2D, tbo);

		int Mode = GL_RGB;

		if (surface->format->BytesPerPixel == 4) {
			Mode = GL_RGBA;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, Mode, surface->w, surface->h, 0, Mode, GL_UNSIGNED_BYTE, flippedRaw);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		SDL_FreeSurface(surface);

		auto error = glGetError();
		assert(error == GL_NO_ERROR);

		if (out_width)
			*out_width = surface->w;
		if (out_height)
			*out_height = surface->h;

		delete[] flippedRaw;

		return tbo;
	}

	uint8_t* Texture::load_raw(const char* filename, int* out_width, int* out_height, int* out_bytes_per_pixel)
	{
		GLuint tbo = 0;
		SDL_Surface* surface = IMG_Load(filename);

		typedef uint8_t byte_type;

		const unsigned bytesPerRow = surface->pitch;
		const byte_type* originalRaw = static_cast<byte_type*>(surface->pixels);
		byte_type* flippedRaw = new byte_type[bytesPerRow * surface->h];

		for (int i = 0; i < surface->h; ++i)
		{
			const byte_type* srcBeg = originalRaw + (bytesPerRow * (surface->h - i - 1));
			const byte_type* srcEnd = srcBeg + bytesPerRow;

			std::copy(srcBeg, srcEnd, flippedRaw + bytesPerRow * i);
		}

		if (out_width)
			*out_width = surface->w;
		if (out_height)
			*out_height = surface->h;
		if (out_bytes_per_pixel)
			*out_bytes_per_pixel = surface->format->BytesPerPixel;

		SDL_FreeSurface(surface);

		auto error = glGetError();
		assert(error == GL_NO_ERROR);



		return flippedRaw;
	}

}