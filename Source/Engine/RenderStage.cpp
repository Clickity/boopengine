
#include "RenderStage.h"

namespace Boop
{

	void RenderStage::add(Renderer* renderer, int position)
	{
		renderers.insert(renderers.begin() + position, renderer);
	}

	void RenderStage::add_back(Renderer* renderer)
	{
		renderers.push_back(renderer);
	}

	void RenderStage::remove(Renderer* renderer)
	{
		for (auto it = renderers.begin(); it != renderers.end(); ++it)
		{
			if (*it == renderer)
			{
				renderers.erase(it);
				break;
			}
		}
	}

}
