
#include "Network.h"

#include <MessageIdentifiers.h>
#include "Logger.h"


#include <FileOperations.h>
#include <BitStream.h>
#include <Gets.h>

#define CLIENT_PORT 55536
#define SERVER_PORT 55537

using namespace RakNet;

namespace Boop
{

	Network::Network(int thd)
		: requestIDCounter(0)
		, frameUpdate(SystemEventID::FRAME_UPDATE, Channel::SYSTEM, thd, std::bind(&Network::update, this))
		, fIsConnected(false)
		, fIsServer(false)
		, fFileTransferEnabled(false)
		, thread(thd)
	{
		peer = RakNet::RakPeerInterface::GetInstance();
		frameUpdate.repeat();
	}

	Network::~Network()
	{
		RakNet::RakPeerInterface::DestroyInstance(peer);
	}

	void Network::startClient(std::string host)
	{
		RakNet::SocketDescriptor socket(CLIENT_PORT, 0);
		peer->Startup(1, &socket, 1);
		peer->Connect(host.c_str(), SERVER_PORT, 0, 0, 0);
		fIsConnected = false;
		fIsServer = false;
	}

	void Network::startServer(int maxConnections)
	{
		RakNet::SocketDescriptor socket(SERVER_PORT, 0);
		peer->Startup(maxConnections, &socket, 1);
		peer->SetMaximumIncomingConnections(maxConnections);
		fIsConnected = true;
		fIsServer = true;
	}

	void Network::enableFileTransfer()
	{
		peer->AttachPlugin(&fileTransfer);
		peer->SetSplitMessageProgressInterval(9);
		fileTransfer.AddCallback(&fileUploadProgress);
		fileTransfer.StartIncrementalReadThreads(1);
		fFileTransferEnabled = true;
	}

	bool Network::isConnected()
	{
		return fIsConnected;
	}

	bool Network::isServer()
	{
		return fIsServer;
	}

	void Network::sendFiles(FileTransferStatus* statusOut, const RakNet::SystemAddress& address, const std::vector<std::filesystem::path>& list)
	{
		if (!fFileTransferEnabled || !fIsConnected)
		{
			*statusOut = FAILED;
			return;
		}

		requests.emplace_back();
		TransferRequest& req = requests.back();

		req.reqID = requestIDCounter++;
		req.setID = 0;
		req.status = statusOut;
		req.systemAddress = address;
		*statusOut = IN_PROGRESS;

		for (auto it = list.begin(); it != list.end(); ++it)
		{
			unsigned int fileLen = GetFileLength(it->string().c_str());
			req.fileList.AddFile(it->filename().string().c_str(), it->string().c_str(), 0, fileLen, fileLen, FileListNodeContext(0, 0, 0, 0), true);
		}

		BitStream bs;
		bs.Write<uint8_t>(ID_FILE_SEND_REQUEST);
		bs.Write<uint16_t>(req.reqID);
		peer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, FILE_ORDER_CHANNEL, address, false);
	}

	std::vector<Network::TransferRequest>& Network::getRequests()
	{
		return requests;
	}

	void Network::setDownloadLocation(const char* path)
	{
		downloadLocation = path;
	}

	const std::filesystem::path& Network::getDownloadLocation()
	{
		return downloadLocation;
	}

	RakNet::RakPeerInterface* Network::getPeer()
	{
		return peer;
	}

	RakNet::SystemAddress Network::getConnectionAddress(int i)
	{
		if (fIsConnected)
		{
			if (fIsServer)
			{
				unsigned short numConns;
				peer->GetConnectionList(0, &numConns);
				RakNet::SystemAddress* addresses = new RakNet::SystemAddress[numConns];
				unsigned short num = 0;
				peer->GetConnectionList(addresses, &num);
				assert(i < num);
				RakNet::SystemAddress address = addresses[i];
				delete addresses;

				return address;
			}
			else {
				RakNet::SystemAddress address;
				unsigned short num = 1;
				peer->GetConnectionList(&address, &num);
				return address;
			}
		}
		else {
			return RakNet::SystemAddress();
		}
	}

	unsigned char Network::getPacketIdentifier(Packet* p)
	{
		if ((unsigned char)p->data[0] == ID_TIMESTAMP)
			return (unsigned char)p->data[sizeof(MessageID) + sizeof(RakNet::Time)];
		else
			return (unsigned char)p->data[0];
	}

	void packetDeleter(RakNet::Packet* packet)
	{
		Network::get()->getPeer()->DeallocatePacket(packet);
	}

	void Network::update()
	{
		EventDispatcher* ed = EventDispatcher::get();
		RakNet::Packet* packet;
		for (packet = peer->Receive(); packet; packet = peer->Receive())
		{
			std::shared_ptr<RakNet::Packet> packet_ptr(packet, &packetDeleter);

			uint8_t id = getPacketIdentifier(packet);
			switch (id)
			{
			case ID_NEW_INCOMING_CONNECTION:
				Logger::out() << "New client connected." << std::endl;
				break;

			case ID_CONNECTION_REQUEST_ACCEPTED:
				Logger::out() << "Connected to server." << std::endl;
				fIsConnected = true;
				break;

			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				Logger::out() << "Another client has disconnected." << std::endl;
				break;

			case ID_REMOTE_CONNECTION_LOST:
				Logger::out() << "Another client has lost the connection." << std::endl;
				break;

			case ID_NO_FREE_INCOMING_CONNECTIONS:
				Logger::out() << "The server is full." << std::endl;
				break;

			case ID_DISCONNECTION_NOTIFICATION:
				if (fIsServer) {
					Logger::out() << "A client has disconnected." << std::endl;
				}
				else {
					Logger::out() << "Server has been disconnected." << std::endl;
					fIsConnected = false;
				}
				break;

			case ID_CONNECTION_LOST:
				if (fIsServer) {
					Logger::out() << "A client lost the connection." << std::endl;
				}
				else {
					Logger::out() << "Connection lost." << std::endl;
					fIsConnected = false;
				}
				break;

			case ID_FILE_SEND_REQUEST:
			{
				bool acceptRequest = false;
				uint16_t reqID;
				if (fIsConnected && fFileTransferEnabled) acceptRequest = true;
				BitStream bs(packet->data, packet->length, false);
				bs.SetReadOffset(8);
				bs.Read(reqID);

				if (acceptRequest)
				{
					uint16_t setID = fileTransfer.SetupReceive(&fileDownloadProgress, false, packet->systemAddress);
					BitStream bs;
					bs.Write<uint8_t>(ID_FILE_SEND_ACCEPTED);
					bs.Write(reqID);
					bs.Write(setID);
					peer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, FILE_ORDER_CHANNEL, packet->systemAddress, false);
				}
				else {
					BitStream bs;
					bs.Write<uint8_t>(ID_FILE_SEND_DENIED);
					bs.Write(reqID);
					peer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, FILE_ORDER_CHANNEL, packet->systemAddress, false);
				}
			}
			break;

			case ID_FILE_SEND_ACCEPTED:
			{
				uint16_t reqID;
				uint16_t setID;
				BitStream bs(packet->data, packet->length, false);
				bs.SetReadOffset(8);
				bs.Read(reqID);
				bs.Read(setID);

				TransferRequest* reqElem = 0;
				for (int i = 0; i < requests.size(); ++i)
				{
					if (requests[i].reqID == reqID)
					{
						reqElem = &requests[i];
					}
				}

				if (reqElem)
				{
					reqElem->setID = setID;
					fileTransfer.Send(&reqElem->fileList, peer, reqElem->systemAddress, setID, HIGH_PRIORITY, FILE_ORDER_CHANNEL, &incrementalReadInterface, 2000000);
				}
			}
			break;

			case ID_FILE_SEND_DENIED:
			{
				uint16_t reqID;
				BitStream bs(packet->data, packet->length, false);
				bs.SetReadOffset(8);
				bs.Read(reqID);

				for (auto it = requests.begin(); it != requests.end(); ++it)
				{
					if (it->reqID == reqID)
					{
						*it->status = FAILED;
						requests.erase(it);
					}
				}
			}
			break;
			}

			ed->dispatch(id, Channel::NETWORK, packet_ptr);
		}
	}

}