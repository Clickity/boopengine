#pragma once

#include "Renderer.h"
#include <vector>

namespace Boop
{

	class RenderStage
	{
	public:
		void add(Renderer* renderer, int position = 0);

		void add_back(Renderer* renderer);

		void remove(Renderer* renderer);

		std::vector<Renderer*> renderers;
	};

}