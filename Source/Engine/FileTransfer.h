#pragma once

#include "RakPeerInterface.h"
#include "FileListTransfer.h"
#include "MessageIdentifiers.h"
#include "FileListTransferCBInterface.h"

// These classes supports file download/upload over the network.

namespace Boop
{

	class FileDownloadProgress : public RakNet::FileListTransferCBInterface
	{
	public:

		virtual ~FileDownloadProgress() {}

		bool OnFile(OnFileStruct* onFileStruct) override;
		virtual void OnFileProgress(FileProgressStruct* fps) override;
		virtual bool OnDownloadComplete(DownloadCompleteStruct* dcs) override;
		virtual void OnDereference() override;

	};

	class FileUploadProgress : public RakNet::FileListProgress
	{
	public:

		virtual void OnFilePush(const char* fileName, unsigned int fileLengthBytes, unsigned int offset, unsigned int bytesBeingSent, bool done, RakNet::SystemAddress targetSystem, unsigned short setID) override;
		virtual void OnFilePushesComplete(RakNet::SystemAddress systemAddress, unsigned short setID) override;
		virtual void OnSendAborted(RakNet::SystemAddress systemAddress) override;
	};

}