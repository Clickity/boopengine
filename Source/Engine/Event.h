#pragma once

#include <functional>
#include <stdint.h>
#include "EventDispatcher.h"

using namespace std::placeholders;


#define BIND_BOOPEVENT(func, inst) std::bind(func, inst)
#define BIND_BOOPDATAEVENT(func, inst) std::bind(func, inst, std::placeholders::_1)

namespace Boop
{
	// Sets up a callback event.
	class Event
	{
	public:

		/// @param event_id The event type that calls this callback
		/// @param channel The channel that the event type uses
		/// @param thread_num The thread to call the function back on
		/// @param callback The function to callback
		Event(uint16_t event_id, uint8_t channel, int thread_num, std::function<void()> callback);
		~Event();

		/// Schedules the callback to run onces on the specified thread.
		void run();

		/// Schedules the callback to repeat until removed. The event cannot be queued again until
		/// the callback function has completed.
		void repeat();

		/// Schedules the callback to repeat until removed. The event is polled and will run as
		/// many times as dipsatch is called.
		void poll();

		/// Stops the event from running.
		void remove();

	private:

		void cast_func(void*);

		uint16_t event_id;
		uint8_t channel;
		std::function<void()> user_func;
		int thread_num;
		uint64_t uid;
	};

	// Sets up a callback event that receives data.
	// `Argument` is the datatype to be passed to the callback.
	template<class Argument = void*>
	class DataEvent
	{
	public:

		/// @param event_id The event type that calls this callback
		/// @param channel The channel that the event type uses
		/// @param thread_num The thread to call the function back on
		/// @param callback The function to callback
		DataEvent(uint16_t event_id, uint8_t channel, int thread_num, std::function<void(Argument*)> callback);
		~DataEvent();

		/// Schedules the callback to run onces on the specified thread.
		void run();

		/// Schedules the callback to repeat until removed. The event cannot be queued again until
		/// the callback function has completed.
		void repeat();

		/// Schedules the callback to repeat until removed. The event is polled and will run as
		/// many times as dipsatch is called.
		void poll();

		/// Stops the event from running.
		void remove();

	private:

		void cast_func(void*);

		uint16_t event_id;
		uint8_t channel;
		std::function<void(Argument*)> user_func;
		int thread_num;
		uint64_t uid;
	};

	extern uint64_t uid_counter_g;

	template<class Argument>
	DataEvent<Argument>::DataEvent(uint16_t event_idi, uint8_t channeli, int thread_num_i, std::function<void(Argument*)> callback) :
		event_id(event_idi),
		channel(channeli),
		thread_num(thread_num_i),
		user_func(callback),
		uid(++uid_counter_g)
	{
	}

	template<class Argument>
	DataEvent<Argument>::~DataEvent()
	{
		remove();
	}

	template<class Argument>
	void DataEvent<Argument>::run()
	{
		EventDispatcher::get()->add(event_id, channel, uid, BIND_BOOPDATAEVENT(&DataEvent<Argument>::cast_func, this), thread_num, false, true);
	}

	template<class Argument>
	void DataEvent<Argument>::repeat()
	{
		EventDispatcher::get()->add(event_id, channel, uid, BIND_BOOPDATAEVENT(&DataEvent<Argument>::cast_func, this), thread_num, true, false);
	}

	template<class Argument>
	void DataEvent<Argument>::poll()
	{
		EventDispatcher::get()->add(event_id, channel, uid, BIND_BOOPDATAEVENT(&DataEvent<Argument>::cast_func, this), thread_num, true, true);
	}

	template<class Argument>
	void DataEvent<Argument>::remove()
	{
		EventDispatcher::get()->remove(event_id, channel, uid);
	}

	template<class Argument>
	void DataEvent<Argument>::cast_func(void* p)
	{
		user_func(static_cast<Argument*>(p));
	}


}