
#include "Renderer.h"

namespace Boop
{

	Renderer::Renderer()
		: is_hidden_f(false)
	{

	}

	void Renderer::show()
	{
		is_hidden_f = false;
	}

	void Renderer::hide()
	{
		is_hidden_f = true;
	}

	bool Renderer::is_hidden()
	{
		return is_hidden_f;
	}

}