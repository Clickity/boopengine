#include "Event.h"

namespace Boop
{

	uint64_t uid_counter_g = 1;


	Event::Event(uint16_t event_idi, uint8_t channeli, int thread_num_i, std::function<void()> callback) :
		event_id(event_idi),
		channel(channeli),
		thread_num(thread_num_i),
		user_func(callback),
		uid(++uid_counter_g)
	{

	}

	Event::~Event()
	{
		remove();
	}

	void Event::run()
	{
		EventDispatcher::get()->add(event_id, channel, uid, BIND_BOOPDATAEVENT(&Event::cast_func, this), thread_num, false, true);
	}

	void Event::repeat()
	{
		EventDispatcher::get()->add(event_id, channel, uid, BIND_BOOPDATAEVENT(&Event::cast_func, this), thread_num, true, false);
	}

	void Event::poll()
	{
		EventDispatcher::get()->add(event_id, channel, uid, BIND_BOOPDATAEVENT(&Event::cast_func, this), thread_num, true, true);
	}

	void Event::remove()
	{
		EventDispatcher::get()->remove(event_id, channel, uid);
	}

	void Event::cast_func(void* data)
	{
		user_func();
	}


}