
#include "Utility.h"


#include <stdio.h>
#include <stdarg.h>
#include <cstring>
#include <mutex>
#include <algorithm>


#ifdef _WIN32
#include <Windows.h>
#endif

namespace Boop
{

    Perf::Perf() :
        endn(0)
    {
        start();
    }

    void Perf::start()
    {
        startn = now();
    }

    void Perf::end()
    {
        endn = now();
    }

    double Perf::getTime()
    {
        return static_cast<double>(endn - startn) / 1000000000.0;
    }

    double Perf::getTimeMs()
    {
        return static_cast<double>(endn - startn) / 1000000.0;
    }

    void Perf::print(const char* tag)
    {
        if (!endn)
            end();
        Logger::out() << tag << ": Took " << (static_cast<double>(endn - startn) / 1000000000.0) << "s" << std::endl;
    }

    void Perf::printms(const char* tag)
    {
        if (!endn)
            end();
        Logger::out() << tag << ": Took " << (static_cast<double>(endn - startn) / 1000000.0) << "ms" << std::endl;
    }

    uint64_t now() {
        return std::chrono::duration_cast
            <std::chrono::nanoseconds>
            (std::chrono::system_clock::now()
                .time_since_epoch())
            .count();
    }

    int random(int min, int max)
    {
        int r = rand() % (max - min);
        return min + r;
    }

    float random(float min, float max)
    {
        float r = (float)rand() / (float)RAND_MAX;
        return min + (max - min) * r;
    }

    uint64_t makeuid()
    {
        uint64_t u = static_cast<uint64_t>(rand());
        return u << 32 | static_cast<uint32_t>(rand());
    }


#define MAX_CIRCLE_ANGLE      512
#define HALF_MAX_CIRCLE_ANGLE (MAX_CIRCLE_ANGLE/2)
#define QUARTER_MAX_CIRCLE_ANGLE (MAX_CIRCLE_ANGLE/4)
#define MASK_MAX_CIRCLE_ANGLE (MAX_CIRCLE_ANGLE - 1)

    float fast_cossin_table[MAX_CIRCLE_ANGLE];           // Declare table of fast cosinus and sinus

    void buildFastCosineTable()
    {
        // Build cossin table
        for (int i = 0; i < MAX_CIRCLE_ANGLE; i++)
        {
            fast_cossin_table[i] = (float)sin((double)i * PI / HALF_MAX_CIRCLE_ANGLE);
        }
    }

    void FloatToInt(int* int_pointer, float f)
    {
        //__asm  fld  f
        //__asm  mov  edx, int_pointer
        //__asm  FRNDINT
        //__asm  fistp dword ptr[edx];
        *int_pointer = (int)f;
    }

    float fastcos(float n)
    {
        float f = n * HALF_MAX_CIRCLE_ANGLE / PI;
        int i;
        FloatToInt(&i, f);
        if (i < 0)
        {
            return fast_cossin_table[((-i) + QUARTER_MAX_CIRCLE_ANGLE) & MASK_MAX_CIRCLE_ANGLE];
        }
        else
        {
            return fast_cossin_table[(i + QUARTER_MAX_CIRCLE_ANGLE) & MASK_MAX_CIRCLE_ANGLE];
        }

        assert(0);
    }

    float fastsin(float n)
    {
        float f = n * HALF_MAX_CIRCLE_ANGLE / PI;
        int i;
        FloatToInt(&i, f);
        if (i < 0)
        {
            return fast_cossin_table[(-((-i) & MASK_MAX_CIRCLE_ANGLE)) + MAX_CIRCLE_ANGLE];
        }
        else
        {
            return fast_cossin_table[i & MASK_MAX_CIRCLE_ANGLE];
        }

        assert(0);
    }

    float ApproxAtan(float z)
    {
        const float n1 = 0.97239411f;
        const float n2 = -0.19194795f;
        return (n1 + n2 * z * z) * z;
    }

    float fastatan2(float y, float x)
    {
        const float n1 = 0.97239411f;
        const float n2 = -0.19194795f;
        float result = 0.0f;
        if (x != 0.0f)
        {
            const union { float flVal; uint32_t nVal; } tYSign = { y };
            const union { float flVal; uint32_t nVal; } tXSign = { x };
            if (fabsf(x) >= fabsf(y))
            {
                union { float flVal; uint32_t nVal; } tOffset = { PI };
                // Add or subtract PI based on y's sign.
                tOffset.nVal |= tYSign.nVal & 0x80000000u;
                // No offset if x is positive, so multiply by 0 or based on x's sign.
                tOffset.nVal *= tXSign.nVal >> 31;
                result = tOffset.flVal;
                const float z = y / x;
                result += (n1 + n2 * z * z) * z;
            }
            else // Use atan(y/x) = pi/2 - atan(x/y) if |y/x| > 1.
            {
                union { float flVal; uint32_t nVal; } tOffset = { PI_2 };
                // Add or subtract PI/2 based on y's sign.
                tOffset.nVal |= tYSign.nVal & 0x80000000u;
                result = tOffset.flVal;
                const float z = x / y;
                result -= (n1 + n2 * z * z) * z;
            }
        }
        else if (y > 0.0f)
        {
            result = PI_2;
        }
        else if (y < 0.0f)
        {
            result = -PI_2;
        }
        return result;
    }

    float linearInterpolation(float x1, float x2, float r)
    {
        return x1 + (x2 - x1) * r;
    }

    float cosineInterpolation(float x1, float x2, float r)
    {
        float z = (1 - fastcos(r * PI)) * 0.5f;
        return (x1 * (1 - z) + x2 * z);
    }

}