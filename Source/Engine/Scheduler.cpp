#include "Scheduler.h"
#include <chrono>
#include "Logger.h"
#include "EventDispatcher.h"
#include "EventID.h"

namespace Boop
{

	Scheduler::Scheduler() :
		is_running_flag(false),
		exit_success(true),
		frame_interval(0),
		frame_number(0)
	{
	}

	Scheduler::~Scheduler()
	{
		stop();
	}

	void Scheduler::run_blocking(uint64_t frame_interval_ns)
	{
		frame_interval = frame_interval_ns;
		main_thread_id = std::this_thread::get_id();

		exit_success = false;
		is_running_flag = true;
		frame_number = 0;

		using namespace std::chrono;
		auto cur_time = steady_clock::now();
		while (is_running_flag)
		{
			EventDispatcher::get()->dispatch(SystemEventID::FRAME_UPDATE, Channel::SYSTEM, 0);

			cur_time += nanoseconds::duration(frame_interval);
			auto this_time = steady_clock::now();
			if (cur_time < this_time)
			{
				Logger::out() << "Scheduler.cpp:46 - Time jump reported (" << (this_time - cur_time).count() << "ns)" << std::endl;
				cur_time = this_time + nanoseconds::duration(frame_interval);
			}
			std::this_thread::sleep_until(cur_time);
			++frame_number;
		}

		EventDispatcher::get()->dispatch(SystemEventID::QUIT, Channel::SYSTEM, 0);

		exit_success = true;
	}

	void Scheduler::stop()
	{
		is_running_flag = false;
		if (std::this_thread::get_id() != main_thread_id)
		{
			while (!exit_success) {}
		}
		exit_success = false;
	}

	bool Scheduler::is_running()
	{
		return is_running_flag;
	}

	uint64_t Scheduler::get_localtime()
	{
#ifdef _DEBUG
		if (std::this_thread::get_id() != main_thread_id)
		{
			return 0;
		}
#endif

		return std::chrono::steady_clock::now().time_since_epoch().count();
	}

	double Scheduler::get_frame_delta()
	{
		return frame_interval / 1000000000.0;
	}

}
