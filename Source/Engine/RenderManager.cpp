
#include "RenderManager.h"
#include "Application.h"
#include <assert.h>
#include <GL/glew.h>

namespace Boop
{

	RenderManager::RenderManager(int render_stages)
		: frame_update(FRAME_UPDATE, Channel::SYSTEM, 0, BIND_BOOPEVENT(&RenderManager::update_event, this))
		, lastElapsedTime(1.f)
	{
		frame_update.repeat();
		stages.resize(render_stages);
	}

	void RenderManager::add_renderer(Renderer* renderer, int stage_num)
	{
		mtx.lock();
		stages[stage_num].add(renderer);
		mtx.unlock();
	}

	void RenderManager::remove_renderer(Renderer* renderer, int stage_num)
	{
		mtx.lock();
		stages[stage_num].remove(renderer);
		mtx.unlock();
	}

	RenderStage& RenderManager::get_stage(int stage)
	{
		return stages[stage];
	}

	int RenderManager::get_num_stages()
	{
		return (int)stages.size();
	}

	void RenderManager::draw()
	{
		auto error = glGetError();
		assert(error == GL_NO_ERROR);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		mtx.lock();
		for (auto stage_it = stages.rbegin(); stage_it != stages.rend(); ++stage_it)
		{
			for (auto renderer_it = stage_it->renderers.begin(); renderer_it != stage_it->renderers.end(); ++renderer_it)
			{
				if (!(*renderer_it)->is_hidden())
					(*renderer_it)->draw();
				auto error = glGetError();
				assert(error == GL_NO_ERROR);
			}
		}
		mtx.unlock();
		SDL_GL_SwapWindow(Application::get()->get_window());
	}

	void RenderManager::update_event()
	{
		draw();

		fpstimer.end();
		lastElapsedTime = fpstimer.elapsed_seconds();
		fpstimer.start();
	}

	double RenderManager::getFPS()
	{
		return 1.0 / lastElapsedTime;
	}

}