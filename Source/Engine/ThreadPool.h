#pragma once

#include "WorkerThread.h"
#include <stdint.h>
#include <vector>

namespace Boop
{
	class ThreadPool
	{
	public:

		/// Creates a thread pool with `num_threads`.
		ThreadPool(uint8_t num_threads);
		~ThreadPool();

		/// Gets the thread object from the index.
		WorkerThread& get_thread(uint8_t thread_num);

		/// @returns The number of threads.
		int get_num_threads();

		/// @returns The index of the calling thread.
		int get_this_thread();

	private:

		std::vector<WorkerThread*> workers;
	};

}