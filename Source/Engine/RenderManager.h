#pragma once

#include "GlobalResource.h"
#include "Renderer.h"
#include "RenderStage.h"
#include "Event.h"
#include "EventID.h"
#include <vector>
#include <mutex>
#include "PerfTimer.h"

namespace Boop
{
	/// Handles render order and update events.
	class RenderManager : public GlobalResource<RenderManager>
	{
	public:

		/// @param render_stages Stages are used to enforce order in rendering.
		RenderManager(int render_stages);

		/// @param renderer The renderer to add. 
		/// @param The stage to use.
		void add_renderer(Renderer* renderer, int stage_num = 0);

		/// @param renderer The renderer to add. 
		/// @param The stage to use.
		void remove_renderer(Renderer* renderer, int stage_num = 0);

		/// @returns the fps the renderer is running at.
		double getFPS();

		RenderStage& get_stage(int stage);

		int get_num_stages();

		void draw();



	private:

		void update_event();

		std::vector<RenderStage> stages;
		Event frame_update;
		std::mutex mtx;

		PerfTimer fpstimer;
		double lastElapsedTime;
	};

}