#include "FrameTimer.h"
#include <SDL.h>

namespace Boop
{

	FrameTimer::FrameTimer()
		: last_cycle(0)
		, ret(0.0)
	{

	}

	FrameTimer::~FrameTimer()
	{

	}

	double FrameTimer::elapsed()
	{
		uint64_t this_cycle = SDL_GetPerformanceCounter();
		ret = static_cast<double>(this_cycle - last_cycle) / SDL_GetPerformanceFrequency();
		if (last_cycle == 0) ret = 0.0;
		last_cycle = this_cycle;
		return ret;
	}

	double FrameTimer::last_elapsed()
	{
		return ret;
	}

}