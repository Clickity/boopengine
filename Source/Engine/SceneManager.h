#pragma once

#include "Scene.h"
#include "GlobalResource.h"
#include "EventID.h"
#include "Event.h"

namespace Boop
{

	/// Scene manager that enforces one scene at a time and handles
	/// cleanup of the current scene.
	class SceneManager : public GlobalResource<SceneManager>
	{
	public:

		SceneManager();

		~SceneManager();

		template<typename T, typename... Args>
		T* next_scene(Args... args);

		Scene* get_scene();

	private:

		void on_quit();

		Scene* scene;
		Event quit_event;
	};

	template<typename T, typename... Args>
	T* SceneManager::next_scene(Args... args)
	{
		if (scene)
		{
			delete scene;
		}

		T* next_scene = new T(args...);
		scene = static_cast<Scene*>(next_scene);
		return next_scene;
	}

}