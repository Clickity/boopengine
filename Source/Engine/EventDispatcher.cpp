#include "EventDispatcher.h"
#include <assert.h>

namespace Boop
{

	EventDispatcher::EventDispatcher(int num_events)
		: thread_pool(0)
		, last_event_id(-1)
	{
		event_pool.reserve(num_events);
	}

	void EventDispatcher::set_thread_pool(ThreadPool* thread_pool_i)
	{
		thread_pool = thread_pool_i;
	}

	ThreadPool* EventDispatcher::get_thread_pool()
	{
		return thread_pool;
	}

	void EventDispatcher::dispatch(uint16_t event_id, uint8_t channel, std::shared_ptr<void> data)
	{
		uint32_t id = getCombinedIndex(event_id, channel);
		std::unordered_map<uint32_t, uint32_t>::iterator find_it = binding_map.find(id);
		if (find_it == binding_map.end())
			return;

		event_map_prot.lock();
		PooledList<EventInstance>& list = pool_ref[(*find_it).second];


		PooledList<EventInstance>::Iterator it = list.begin();
		assert(id != last_event_id);
		last_event_id = id;



		while (!it.end())
		{

			if (!thread_pool || thread_pool->get_num_threads() == 0 || it->thread_num == THIS_THREAD)
			{
				it->func(data.get());
			}
			else {
				int thisThread = thread_pool->get_this_thread();
				if (it->thread_num == thisThread)
				{
					it->func(data.get());
				}
				else {
					int run_on_thread = it->thread_num;
					if (run_on_thread == ANY_THREAD)
					{
						run_on_thread = 0;
						int least_num = thread_pool->get_thread(0).num_runners();
						for (uint8_t i = 1; i < thread_pool->get_num_threads(); ++i)
						{
							if (thread_pool->get_thread(i).num_runners() < least_num)
							{
								run_on_thread = i;
								least_num = thread_pool->get_thread(i).num_runners();
							}
						}
					}

					if (it->can_duplicate || it->finished_flag)
					{
						thread_pool->get_thread(run_on_thread).run_this(it->func, data, &(it->finished_flag), it->uid);
					}

				}
			}

			if (!it->is_repeating)
			{
				list.remove(it);
			}
			else {
				++it;
			}
		}
		last_event_id = -1;
		event_map_prot.unlock();
	}

	void EventDispatcher::add(uint16_t event_id, uint8_t channel, uint64_t uid, EventFunction func, int thread_num, bool is_repeating, bool can_duplicate)
	{
		uint32_t id = getCombinedIndex(event_id, channel);

		EventInstance e;
		e.func = func;
		e.is_repeating = is_repeating;
		e.thread_num = thread_num;
		e.uid = uid;
		e.can_duplicate = can_duplicate;
		e.finished_flag = true;

		event_map_prot.lock();
		std::unordered_map<uint32_t, uint32_t>::iterator find_it = binding_map.find(id);
		int ref_index;
		if (find_it == binding_map.end())
		{
			ref_index = (int)pool_ref.size();
			pool_ref.emplace_back(&event_pool);
			binding_map[id] = ref_index;
		}
		else {
			ref_index = (*find_it).second;
		}
		if (can_duplicate)
			pool_ref[ref_index].push_front(e);
		else
			pool_ref[ref_index].push_back(e);
		event_map_prot.unlock();
	}

	bool EventDispatcher::remove(uint16_t event_id, uint8_t channel, uint64_t uid)
	{
		uint32_t id = getCombinedIndex(event_id, channel);

		event_map_prot.lock();
		std::unordered_map<uint32_t, uint32_t>::iterator find_it = binding_map.find(id);
		int ref_index;
		if (find_it == binding_map.end())
		{
			return false;
		}
		else {
			ref_index = (*find_it).second;
		}

		bool ret = false;
		PooledList<EventInstance>& pool = pool_ref[ref_index];
		for (PooledList<EventInstance>::Iterator it = pool.begin(); !it.end(); ++it)
		{
			if (it->uid == uid)
			{
				ret = true;
				pool.remove(it);
			}
		}

		event_map_prot.unlock();

		return ret;
	}


	void EventDispatcher::run_this(int thread_num, std::function<void(void*)> func, std::shared_ptr<void> data, bool* finished_flag)
	{
		if (!thread_pool || thread_pool->get_num_threads() == 0 || thread_num == THIS_THREAD)
		{
			func(data.get());
		}
		else {
			int run_on_thread = thread_num;
			if (run_on_thread == ANY_THREAD)
			{
				run_on_thread = 0;
				int least_num = thread_pool->get_thread(0).num_runners();
				for (uint8_t i = 1; i < thread_pool->get_num_threads(); ++i)
				{
					if (thread_pool->get_thread(i).num_runners() < least_num)
					{
						run_on_thread = i;
						least_num = thread_pool->get_thread(i).num_runners();
					}
				}
			}

			thread_pool->get_thread(run_on_thread).run_this(func, data);
		}
	}

	uint32_t EventDispatcher::getCombinedIndex(uint16_t event_id, uint8_t channel)
	{
		uint32_t val = channel;
		val = val << 16;
		val = val | event_id;
		return val;
	}

}