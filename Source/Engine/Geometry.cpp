
#include "Geometry.h"

#include <cmath>
#include <algorithm>

namespace Boop
{

	void Geometry::generate_box2d(std::vector<glm::vec2>* out_verts, const glm::vec2& lower_left, const glm::vec2& upper_right)
	{
		out_verts->push_back(glm::vec2(lower_left.x, upper_right.y));
		out_verts->push_back(glm::vec2(lower_left.x, lower_left.y));
		out_verts->push_back(glm::vec2(upper_right.x, lower_left.y));

		out_verts->push_back(glm::vec2(upper_right.x, lower_left.y));
		out_verts->push_back(glm::vec2(upper_right.x, upper_right.y));
		out_verts->push_back(glm::vec2(lower_left.x, upper_right.y));
	}

	void Geometry::generate_box2d(std::vector<glm::vec2>* out_verts, const glm::vec2& lower_left, const glm::vec2& upper_right, int horizontal_divisions, int vertical_divisions)
	{
		float width = upper_right.x - lower_left.x;
		float height = upper_right.y - lower_left.y;

		float dx = width / horizontal_divisions;
		float dy = height / vertical_divisions;

		for (int ix = 0; ix < horizontal_divisions; ++ix)
		{
			float x = lower_left.x + dx * ix;
			float x2 = x + dx;
			for (int iy = 0; iy < vertical_divisions; ++iy)
			{
				float y = lower_left.y + dy * iy;
				float y2 = y + dy;
				out_verts->push_back(glm::vec2(x, y2));
				out_verts->push_back(glm::vec2(x, y));
				out_verts->push_back(glm::vec2(x2, y));

				out_verts->push_back(glm::vec2(x2, y));
				out_verts->push_back(glm::vec2(x2, y2));
				out_verts->push_back(glm::vec2(x, y2));
			}
		}


	}

	void Geometry::sub_divide(std::vector<glm::vec2>* out_verts)
	{
		size_t size = out_verts->size();
		for (int i = 0; i < size; i += 3)
		{
			glm::vec2 p1 = out_verts->at(i);
			glm::vec2 p2 = out_verts->at(i + 1);
			glm::vec2 p3 = out_verts->at(i + 2);
			glm::vec2 mid1 = half_point(p1, p2);
			glm::vec2 mid2 = half_point(p2, p3);
			glm::vec2 mid3 = half_point(p3, p1);

			(*out_verts)[i] = mid1;
			(*out_verts)[i + 1] = mid2;
			(*out_verts)[i + 2] = mid3;

			out_verts->push_back(p1);
			out_verts->push_back(mid1);
			out_verts->push_back(mid3);

			out_verts->push_back(p2);
			out_verts->push_back(mid2);
			out_verts->push_back(mid1);

			out_verts->push_back(p3);
			out_verts->push_back(mid3);
			out_verts->push_back(mid2);
		}
	}

	void Geometry::sub_divide(std::vector<glm::vec2>* out_verts, int n)
	{
		for (int i = 0; i < n; ++i)
		{
			sub_divide(out_verts);
		}
	}


	float Geometry::distance2(const glm::vec2& p1, const glm::vec2& p2)
	{
		return (p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y);
	}


	float Geometry::distance(const glm::vec2& p1, const glm::vec2& p2)
	{
		return sqrt(distance2(p1, p2));
	}

	glm::vec2 Geometry::half_point(const glm::vec2& p1, const glm::vec2& p2)
	{
		return glm::vec2((p1.x + p2.x) * 0.5f, (p1.y + p2.y) * 0.5f);
	}

	float Geometry::linear_weight_gradient(float value, float lower_value, float upper_value)
	{
		float r = (value - lower_value) / (upper_value - lower_value);
		return std::clamp(r, 0.0f, 1.0f);
	}

	float Geometry::multivariable_composite_weight_gradient(float v1, float v2, float lv1, float uv1, float lv2, float uv2)
	{
		float r1 = (v1 - lv1) / (uv1 - lv1);
		float r2 = (v2 - lv2) / ((uv2 - lv2) * r1);
		return std::clamp(r2, 0.0f, 1.0f);
	}

	float Geometry::joint_weight_gradient(float v1, float v2, float lv1, float uv1, float lv2, float uv2, float pinch_size, float power)
	{
		float r2 = abs(((v2 - lv2) / (uv2 - lv2) - 0.5f) * 2.0f);
		float v1_center = (lv1 + uv1) * 0.5f;
		float lv3 = lerp(v1_center, lv1, r2 + pinch_size);
		float uv3 = lerp(v1_center, uv1, r2 + pinch_size);
		float r1 = (v1 - lv3) / (uv3 - lv3);

		return pow(std::clamp(r1, 0.0f, 1.0f), power);
	}

	float Geometry::lerp(float x1, float x2, float r)
	{
		return (x2 - x1) * r + x1;
	}


	bool Geometry::intersect_rect_rect(const glm::vec2& min1, const glm::vec2& max1, const glm::vec2& min2, const glm::vec2& max2)
	{
		return true;
	}

	bool Geometry::intersect_point_rect(const glm::vec2& point, const glm::vec2& min, const glm::vec2& max)
	{
		return (point.x <= max.x && point.x >= min.x) &&
			(point.y <= max.y && point.y >= min.y);
	}

}