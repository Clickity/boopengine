#pragma once

// A global resource that can be accessed anywhere. The object must be created
// and only once.
//
// The pattern is similar to a singleton, except it requires specific creation.
template<typename T>
class GlobalResource
{
public:

	template<typename... Args>
	static T* create(Args... args);

	static T* get();

	static void destroy();

	static bool is_initialized();
	static void set_initialized(bool v);

protected:


private:
	static T* instance;
	static bool is_initialized_flag;
};


template<typename T>
T* GlobalResource<T>::instance = 0;

template<typename T>
bool GlobalResource<T>::is_initialized_flag = false;


template<typename T>
template<typename... Args>
T* GlobalResource<T>::create(Args... args)
{
	instance = new T(args...);
	
	return instance;
}

template<typename T>
T* GlobalResource<T>::get()
{
	return instance;
}

template<typename T>
void GlobalResource<T>::destroy()
{
	if (instance != 0)
	{
		is_initialized_flag = false;
		delete instance;
	}

}

template<typename T>
bool GlobalResource<T>::is_initialized() 
{
	return is_initialized_flag;
}

template<typename T>
void GlobalResource<T>::set_initialized(bool v)
{
	is_initialized_flag = v;
}
