#include "WorkerThread.h"
#include "Logger.h"
#include "PerfTimer.h"
#include <assert.h>

namespace Boop
{

	int WorkerThread::thread_cnt = 1;

	WorkerThread::WorkerThread() 
		: is_running(true)
		, exit_success(false)
		, running_uid(-1)
	{
		thd = std::thread(&WorkerThread::start, this);
		thread_n = thread_cnt;
		++thread_cnt;
	}

	WorkerThread::~WorkerThread()
	{
		stop();
	}

	void WorkerThread::start()
	{
		Logger::out() << "Worker thread " << thread_n << " started." << std::endl;

		const int MAX_RUNNERS = 10000;
		thread_id = std::this_thread::get_id();
		while (is_running)
		{
			std::unique_lock<std::mutex> lck(runner_mtx); // locks
			while (runners.size() == 0 && is_running) sleeper.wait(lck); // unlocks on wait, locks on return
			lck.unlock();
			while (runners.size() > 0)
			{
				lck.lock();
				RunInstance inst = runners.front();
				runners.pop_front();
				running_uid = inst.uid;
				lck.unlock();

				if (inst.data)
					inst.func(inst.data.get());
				else
					inst.func(0);

				if (inst.finished)
					*(inst.finished) = true;
				running_uid = -1;

				assert(runners.size() < MAX_RUNNERS); // Notifies if a thread is overrunning.
			}

		}
		exit_success = true;
		Logger::out() << "Worker Thread " << thread_n << " shutdown." << std::endl;
	}

	void WorkerThread::stop()
	{
		is_running = false;
		if (std::this_thread::get_id() != thread_id)
		{
			while (!exit_success)
			{
				sleeper.notify_all();
			}
		}
		thd.join();
	}

	void WorkerThread::run_this(std::function<void(void*)> func, std::shared_ptr<void> data, bool* finished_flag, uint64_t uid)
	{
		RunInstance inst;
		inst.data = data;
		inst.func = func;
		inst.finished = finished_flag;
		inst.uid = uid;
		if (inst.finished)
			*(inst.finished) = false;
		runner_mtx.lock();
		runners.push_back(inst);
		runner_mtx.unlock();
		sleeper.notify_all();
	}

	void WorkerThread::remove(uint64_t uid)
	{
		runner_mtx.lock();
		for (auto it = runners.begin(); it != runners.end(); ++it)
		{
			if (it->uid == uid)
			{
				runners.erase(it);

				if (thread_id != std::this_thread::get_id())
				{
					while (uid == running_uid) // uid running in another thread, wait for it
					{
					}
				}

				break;
			}
		}
		runner_mtx.unlock();
	}

	int WorkerThread::num_runners()
	{
		return static_cast<int>(runners.size());
	}

	std::thread::id WorkerThread::get_thread_id()
	{
		return thread_id;
	}

}