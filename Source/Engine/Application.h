#pragma once

#include "ApplicationSettings.h"

#include <SDL.h>
#include "GlobalResource.h"

namespace Boop
{
	class Event;

	class Application : public GlobalResource<Application>
	{
	public:
		Application(const ApplicationSettings& settings_i);
		~Application();

		/// @returns The SDL window handle
		SDL_Window* get_window();

		/// @returns The SDL opengl context
		SDL_GLContext get_gl_context();

		/// @returns window width
		int get_width();

		/// @returns window height
		int get_height();

		/// @returns mouse position X
		int get_mouse_x();

		/// @returns mouse position Y
		int get_mouse_y();

		/// @returns True if key is currently pressed
		bool isKeyDown(SDL_Scancode key);

	private:

		void deferred_init(void*);

		void event_handler();

		Event* periodic_event;
		SDL_Window* window;
		SDL_GLContext gl_context;
		int scheduled_thread;
		ApplicationSettings settings;
	};

}