#pragma once

#include <new>
#include <string>
#include <assert.h>

/// Base class for a simple Stack container.
template<class T>
class StackBase
{
public:
	class Iterator
	{
	public:
		Iterator& operator++();
		Iterator operator++(int);
		T& operator*();
		bool end();

		T* e;
		T* last;
	};

	StackBase();
	void push(const T& elem);
	void pushSafe(const T& elem);
	template<class ... Params>
	void emplace(Params ... args);
	void clear() ;
	bool isFull();
	T& operator[] (int n);
	const T& operator[] (int n) const;
	T& at(int n);
	T& back();
	void pop();
	Iterator begin();
	int size() const;
	virtual T* data() = 0;
	virtual const T* data() const = 0;
	virtual int capacity() = 0;
	virtual void reserve(int n) = 0;

private:
	int end;
};


/// Static version of a stack. `pushSafe` will add an element only if space is available.
template<class T, int Size>
class StaticStack : public StackBase<T>
{
public:

	StaticStack();
	T* data() override;
	const T* data() const override;
	int capacity() override;
	void reserve(int n) override;
	StackBase<T>* getBase();

private:
	T elements[Size];
};

/// Dynamic version of a stack. Most cases it makes more sense just to use `std::vector` as it
/// will have more features.
template<class T>
class DynamicStack : public StackBase<T>
{
public:

	DynamicStack();
	~DynamicStack();
	T* data() override;
	const T* data() const override;
	int capacity() override;
	void reserve(int n) override;

private:
	T* elements;
	int arraySize;
};



template<class T>
StackBase<T>::StackBase()
	: end(0)
{
	
}

template<class T>
void StackBase<T>::push(const T& elem)
{
	if (end >= capacity()) reserve(capacity() * 2 + 1);
	data()[end] = elem;
	++end;
}

template<class T>
void StackBase<T>::pushSafe(const T& elem)
{
	if (end >= capacity()) return;
	data()[end] = elem;
	++end;
}

template<class T>
template<class ... Params>
void StackBase<T>::emplace(Params ... args)
{
	if (end >= capacity()) reserve(capacity() * 2 + 1);
	T* location = data() + end;
	new (location) T(args...);
	++end;
}

template<class T>
void StackBase<T>::clear()
{
	end = 0;
}

template<class T>
int StackBase<T>::size() const
{
	return end;
}

template<class T>
bool StackBase<T>::isFull()
{
	return end >= capacity();
}

template<class T>
T& StackBase<T>::operator[] (int n)
{
	return data()[n];
}

template<class T>
const T& StackBase<T>::operator[] (int n) const
{
	return data()[n];
}

template<class T>
T& StackBase<T>::at(int n)
{
	return data()[n];
}

template<class T>
T& StackBase<T>::back()
{
	return data()[size() - 1];
}

template<class T>
void StackBase<T>::pop()
{
	--end;
}

template<class T>
typename StackBase<T>::Iterator StackBase<T>::begin()
{
	Iterator it;
	it.e = data();
	it.last = data() + size() - 1;
	return it;
}

template<class T>
typename StackBase<T>::Iterator& StackBase<T>::Iterator::operator++()
{
	++e;
	return *this;
}

template<class T>
typename StackBase<T>::Iterator StackBase<T>::Iterator::operator++(int)
{
	Iterator it = *this;
	operator++();
	return it;
}

template<class T>
T& StackBase<T>::Iterator::operator*()
{
	return *e;
}

template<class T>
bool StackBase<T>::Iterator::end()
{
	return e > last;
}


template<class T, int Size>
StaticStack<T, Size>::StaticStack()
{
}

template<class T, int Size>
T* StaticStack<T, Size>::data()
{
	return elements;
}

template<class T, int Size>
const T* StaticStack<T, Size>::data() const
{
	return elements;
}

template<class T, int Size>
int StaticStack<T, Size>::capacity()
{
	return Size;
}

template<class T, int Size>
void StaticStack<T, Size>::reserve(int n)
{
	assert(false);
}

template<class T, int Size>
StackBase<T>* StaticStack<T, Size>::getBase()
{
	return (StackBase<T>*)this;
}

template<class T>
DynamicStack<T>::DynamicStack()
	: elements(0)
	, arraySize(0)
{

}

template<class T>
DynamicStack<T>::~DynamicStack()
{
	if (arraySize > 0)
	{
		delete[] elements;
	}
}

template<class T>
T* DynamicStack<T>::data()
{
	return elements;
}

template<class T>
const T* DynamicStack<T>::data() const
{
	return elements;
}

template<class T>
int DynamicStack<T>::capacity()
{
	return arraySize;
}

template<class T>
void DynamicStack<T>::reserve(int n)
{
	T* old = elements;
	elements = new T[n];
	if (arraySize > 0)
	{
		memcpy(elements, old, arraySize * sizeof(T));
		delete[] old;
	}
	arraySize = n;
}