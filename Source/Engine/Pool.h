#pragma once

#include <new>
#include <assert.h>

// This class supports Static and Dynamic pools.


/// Base class for pool variants.
/// It's useful to cast to this if you don't care about the
/// derived pool type.
template<class T>
class PoolBase
{
public:
	struct Element
	{
		T val;
		Element* next;
	};

	/// Different from C++ iterators in that it.end() is how you
	/// determine the end.
	struct Iterator
	{
		Iterator& operator++();
		Iterator operator++(int);
		T& operator*();
		T* operator->();
		T& get();
		bool end();

		Element* e;
		Element* last;
	};

	PoolBase();
	~PoolBase();
	T* insert(const T& elem);
	template<class ... Params>
	T* emplace(Params ... args);
	void remove(T* elem);
	void clear();
	int size();
	Iterator begin();
	virtual Element* data() = 0;
	virtual int capacity() = 0;

protected:

	void setup();
	void teardown();

private:
	Element* free;
	int num;
};

/// Static version of a Pool allocator.
template<class T, int Size>
class StaticPool : public PoolBase<T>
{
public:

	StaticPool();
	~StaticPool();
	PoolBase<T>::Element* data() override;
	int capacity() override;

private:
	PoolBase<T>::Element elements[Size];
};

/// Heap allocated version of a Pool allocator. Resizing is not implemented.
template<class T>
class DynamicPool : public PoolBase<T>
{
public:

	DynamicPool();
	~DynamicPool();
	PoolBase<T>::Element* data() override;
	int capacity() override;
	void reserve(int n);

private:
	PoolBase<T>::Element* elements;
	int arraySize;
};

template<class T>
PoolBase<T>::PoolBase()
{

}

template<class T>
PoolBase<T>::~PoolBase()
{
}

template<class T>
T* PoolBase<T>::insert(const T& elem)
{
	T* ret = &free->val;
	Element* next_free = free->next;
	free->next = 0;
	free->val = elem;
	free = next_free;
	++num;
	return ret;
}

template<class T>
template<class ... Params>
T* PoolBase<T>::emplace(Params ... args)
{
	T* ret = &free->val;
	Element* next_free = free->next; // Fails here if hit capacity
	free->next = 0;
	new (ret) T(args...);
	free = next_free;
	++num;
	return ret;
}

template<class T>
void PoolBase<T>::remove(T* elem)
{
	Element* e = reinterpret_cast<Element*>(elem);
	assert(e >= data() && e < data()+capacity() && e->next == 0);
	e->next = free;
	free = e;
	e->val.~T();
	--num;
}

template<class T>
void PoolBase<T>::setup()
{
	for (int i = 0; i < capacity() - 1; ++i)
	{
		data()[i].next = data() + i + 1;
	}
	data()[capacity() - 1].next = (Element*)1; // Will error if run out of memory
	free = data();
	num = 0;
}

template<class T>
void PoolBase<T>::teardown()
{
	for (int i = 0; i < capacity() - 1; ++i)
	{
		Element& e = data()[i];
		if (e.next != 0)
		{
			e.val.~T();
		}
	}
}

template<class T>
void PoolBase<T>::clear()
{
	teardown();
	setup();
}

template<class T>
int PoolBase<T>::size()
{
	return num;
}

template<class T>
typename PoolBase<T>::Iterator PoolBase<T>::begin()
{
	Iterator it;
	it.e = data();
	it.last = data() + capacity() - 1;
	if (it.e->next != 0)
	{
		++it;
	}
	return it;
}

template<class T>
typename PoolBase<T>::Iterator& PoolBase<T>::Iterator::operator++()
{
	do
	{
		++e;
	} while (!end() && e->next != 0);

	return *this;
}

template<class T>
typename PoolBase<T>::Iterator PoolBase<T>::Iterator::operator++(int)
{
	Iterator it = *this;
	operator++();
	return it;
}

template<class T>
T& PoolBase<T>::Iterator::operator*()
{
	return e->val;
}

template<class T>
T* PoolBase<T>::Iterator::operator->()
{
	return &e->val;
}

template<class T>
T& PoolBase<T>::Iterator::get()
{
	return e->val;
}

template<class T>
bool PoolBase<T>::Iterator::end()
{
	return e > last;
}

template<class T, int Size>
StaticPool<T, Size>::StaticPool()
{
	PoolBase<T>::setup();
}

template<class T, int Size>
StaticPool<T, Size>::~StaticPool()
{
	PoolBase<T>::teardown();
}

template<class T, int Size>
typename PoolBase<T>::Element* StaticPool<T, Size>::data()
{
	return elements;
}

template<class T, int Size>
int StaticPool<T, Size>::capacity()
{
	return Size;
}

template<class T>
DynamicPool<T>::DynamicPool()
	: elements(0)
	, arraySize(0)
{

}

template<class T>
DynamicPool<T>::~DynamicPool()
{
	PoolBase<T>::teardown();
	if (arraySize > 0)
	{
		delete[] elements;
		elements = 0;
		arraySize = 0;
	}
}

template<class T>
typename PoolBase<T>::Element* DynamicPool<T>::data()
{
	return elements;
}

template<class T>
int DynamicPool<T>::capacity()
{
	return arraySize;
}

template<class T>
void DynamicPool<T>::reserve(int n)
{
	if (arraySize > 0)
	{
		typename PoolBase<T>::clear();
		delete[] elements;
	}
	elements = new typename PoolBase<T>::Element[n];
	arraySize = n;
	PoolBase<T>::setup();
}
