#pragma once

#include <vector>
#include <glm/glm.hpp>

// Geometry helper functions

namespace Boop
{

	namespace Geometry
	{

		void generate_box2d(std::vector<glm::vec2>* out_verts, const glm::vec2& lower_left, const glm::vec2& upper_right);

		void generate_box2d(std::vector<glm::vec2>* out_verts, const glm::vec2& lower_left, const glm::vec2& upper_right, int horizontal_divisions, int vertical_divisions);

		void sub_divide(std::vector<glm::vec2>* out_verts);

		void sub_divide(std::vector<glm::vec2>* out_verts, int n);

		float distance2(const glm::vec2& p1, const glm::vec2& p2);

		float distance(const glm::vec2& p1, const glm::vec2& p2);

		glm::vec2 half_point(const glm::vec2& p1, const glm::vec2& p2);

		float linear_weight_gradient(float value, float lower_value, float upper_value);

		float multivariable_composite_weight_gradient(float v1, float v2, float lv1, float upv1, float lv2, float uv2);

		float joint_weight_gradient(float v1, float v2, float lv1, float upv1, float lv2, float uv2, float pinch_size = 0.1f, float power = 1.0f);

		float lerp(float x1, float x2, float r);

		bool intersect_rect_rect(const glm::vec2& min1, const glm::vec2& max1, const glm::vec2& min2, const glm::vec2& max2);

		bool intersect_point_rect(const glm::vec2& point, const glm::vec2& min, const glm::vec2& max);

	}

}