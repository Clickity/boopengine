#pragma once

#include "Renderer.h"

namespace Boop
{

	class GUIRenderer : public Renderer
	{
	public:
		void draw() override;

	};

}