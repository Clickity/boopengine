#pragma once

#include <stdint.h>
#include <string>

namespace Boop
{

	struct ApplicationSettings
	{
		/// Start application maximized?
		bool is_maximized;

		/// Start application fullscreen?
		bool is_fullscreen;

		/// Can window be resized?
		bool is_resizable;

		/// Use SDL_WINDOW_FULLSCREEN_DESKTOP instead of SDL_WINDOW_FULLSCREEN?
		bool is_default_resolution;

		/// Name of the window
		std::string application_name;

		/// Position X of the window
		uint32_t window_offset_x;

		/// Position Y of hte window
		uint32_t window_offset_y;

		/// Window width
		uint32_t window_width;

		/// Window height
		uint32_t window_height;

		/// The thread to run application events on. These are a mix of SYSTEM and SDL events.
		int32_t thread_num;
	};

}