#pragma once

#include <GlobalResource.h>
#include <fstream>
#include <ostream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>
#include <list>
#include <unordered_map>

// A logger usable in high performance code sections because
// stream input only buffers for bacakground thread.
//
// Outputs for windows, console, and file.

namespace Boop
{

	class Logger;

	class OutputBuffer : public std::stringbuf
	{
	public:
		OutputBuffer(Logger* logger);

		int sync() override;

	private:
		Logger* logger;

	};

	class Logger : public GlobalResource<Logger>
	{
	public:
		Logger();
		~Logger();

		/// Use to get the stream to output to.
		static std::ostream& out();

		/// Enable/disable console logging. Default on.
		void enableConsoleLog();
		void disableConsoleLog();

		/// Enable/disable file logging. Default off.
		void enableFileLog(const char* filename, bool overwrite = false);
		void disableFileLog();

		/// Not implemented.
		void enableNetworkLog(const char* filename);
		void disableNetworkLog();

		std::ostream& getStream();
		void pushString(const std::string& str);

	private:
		void bgThread();

		struct Buffer : public std::ostream
		{
			Buffer(Logger* l);

			OutputBuffer buffer;
		};

		bool running;
		std::unordered_map<std::thread::id, Buffer*> bufferMap;
		std::thread thd;
		std::list<std::string> outputStrings;
		std::mutex runner_mtx;
		std::condition_variable sleeper;
		std::ofstream* outFile;

		bool fWriteConsole;
		bool fWriteFile;
		bool fWriteNetwork;
	};

}