#pragma once

#include <GL/glew.h>
#include <SDL_image.h>

namespace Boop
{

	/// Helper class for loading textures.
	/// The static classes here are an alternative to using this class as an instance.
	class Texture
	{
	public:

		/// @param filename Loads OpenGL texture from file.
		Texture(const char* filename);

		/// Creates a on pixel texture with the specified color.
		Texture(float r, float g, float b, float a);

		/// Cleans up texture handle.
		~Texture();

		/// @returns The texture buffer object.
		GLuint get_buffer();

		/// @param filename The filename to load.
		/// @param out_width The width of the loaded texture.
		/// @param out_height The height of the loaded texture.
		/// @returns The texture buffer object.
		static GLuint load_file(const char* filename, int* out_width = 0, int* out_height = 0);

		/// @param filename The filename to load.
		/// @param out_width The width of the loaded texture.
		/// @param out_height The height of the loaded texture.
		/// @returns The color data from the file. MUST CALL DELETE[] ON RETURNED VALUE!
		static uint8_t* load_raw(const char* filename, int* out_width = 0, int* out_height = 0, int* out_bytes_per_pixel = 0);

	private:

		GLuint tbo;

	};

}