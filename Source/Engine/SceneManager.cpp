
#include "SceneManager.h"
#include "Camera.h"

namespace Boop
{

	SceneManager::SceneManager()
		: scene(0)
		, quit_event(QUIT, Channel::SYSTEM, THIS_THREAD, std::bind(&SceneManager::on_quit, this))
	{
	}

	SceneManager::~SceneManager()
	{
	}

	Scene* SceneManager::get_scene()
	{
		return scene;
	}

	void SceneManager::on_quit()
	{
		if (scene)
			delete scene;
	}

}