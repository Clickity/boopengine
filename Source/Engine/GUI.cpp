
#include "Logger.h"
#include "GUI.h"
#include "Application.h"
#include "EventDispatcher.h"
#include "Event.h"
#include "EventID.h"
#include "Scheduler.h"
#include "RenderManager.h"
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

namespace Boop
{

    GUI::GUI(const GUISettings& settings_i)
        : mouseenter_event(0)
        , mouseleave_event(0)
        , mousemotion_event(0)
        , mousedown_event(0)
        , mouseup_event(0)
        , mousewheel_event(0)
        , keydown_event(0)
        , keyup_event(0)
        , textinput_event(0)
        , videoresize_event(0)
        , frame_event(0)
        , guirenderer(0)
    {
        settings = settings_i;

        EventDispatcher::get()->run_this(settings.thread_num, BIND_BOOPDATAEVENT(&GUI::deferred_init, this));
    }

    GUI::~GUI()
    {
        delete mouseenter_event;
        delete mouseleave_event;
        delete mousemotion_event;
        delete mousedown_event;
        delete mouseup_event;
        delete mousewheel_event;
        delete keydown_event;
        delete keyup_event;
        delete textinput_event;
        delete videoresize_event;
        delete frame_event;

        CEGUI::System::destroy();
        CEGUI::OpenGL3Renderer::destroy(*guirenderer);
    }

    void GUI::deferred_init(void*)
    {
        Application* app = Application::get();
        if (!app->is_initialized())
        {
            Logger::out() << "GUI couldn't initialize." << std::endl;
            return;
        }

        init_key_map();

        guirenderer = &CEGUI::OpenGL3Renderer::create();
        CEGUI::System::create(*guirenderer);

        SDL_ShowCursor(SDL_DISABLE);

        CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>
            (CEGUI::System::getSingleton().getResourceProvider());

        if (settings.resource_directory[settings.resource_directory.size() - 1] != '/')
            settings.resource_directory.append("/");

        rp->setResourceGroupDirectory("schemes", settings.resource_directory + "schemes");
        rp->setResourceGroupDirectory("imagesets", settings.resource_directory + "imagesets");
        rp->setResourceGroupDirectory("fonts", settings.resource_directory + "fonts");
        rp->setResourceGroupDirectory("layouts", settings.resource_directory + "layouts");
        rp->setResourceGroupDirectory("looknfeels", settings.resource_directory + "looknfeel");
        rp->setResourceGroupDirectory("lua_scripts", settings.resource_directory + "lua_scripts");

        CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
        CEGUI::Font::setDefaultResourceGroup("fonts");
        CEGUI::Scheme::setDefaultResourceGroup("schemes");
        CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
        CEGUI::WindowManager::setDefaultResourceGroup("layouts");
        CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");

        mouseenter_event = new DataEvent<SDL_Event>(SDL_WINDOWEVENT_ENTER, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_mouseenter, this));
        mouseenter_event->poll();

        mouseleave_event = new DataEvent<SDL_Event>(SDL_WINDOWEVENT_LEAVE, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_mouseleave, this));
        mouseleave_event->poll();

        mousemotion_event = new DataEvent<SDL_Event>(SDL_MOUSEMOTION, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_mousemotion, this));
        mousemotion_event->poll();

        mousedown_event = new DataEvent<SDL_Event>(SDL_MOUSEBUTTONDOWN, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_mousedown, this));
        mousedown_event->poll();

        mouseup_event = new DataEvent<SDL_Event>(SDL_MOUSEBUTTONUP, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_mouseup, this));
        mouseup_event->poll();

        mousewheel_event = new DataEvent<SDL_Event>(SDL_MOUSEWHEEL, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_mousewheel, this));
        mousewheel_event->poll();

        keydown_event = new DataEvent<SDL_Event>(SDL_KEYDOWN, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_keydown, this));
        keydown_event->poll();

        keyup_event = new DataEvent<SDL_Event>(SDL_KEYUP, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_keyup, this));
        keyup_event->poll();

        textinput_event = new DataEvent<SDL_Event>(SDL_TEXTINPUT, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_textinput, this));
        textinput_event->poll();

        videoresize_event = new DataEvent<SDL_Event>(SDL_WINDOWEVENT_SIZE_CHANGED, Channel::SDL, settings.thread_num, BIND_BOOPDATAEVENT(&GUI::inject_videoresize, this));
        videoresize_event->poll();

        frame_event = new Event(FRAME_UPDATE, Channel::SDL, 0, BIND_BOOPEVENT(&GUI::inject_frame_event, this));
        frame_event->repeat();

        RenderManager::get()->add_renderer(&renderer);


        set_initialized(true);
        Logger::out() << "GUI Initialized." << std::endl;

    }

    void GUI::init_key_map()
    {
        keymap[SDLK_1] = CEGUI::Key::One;
        keymap[SDLK_2] = CEGUI::Key::Two;
        keymap[SDLK_3] = CEGUI::Key::Three;
        keymap[SDLK_4] = CEGUI::Key::Four;
        keymap[SDLK_5] = CEGUI::Key::Five;
        keymap[SDLK_6] = CEGUI::Key::Six;
        keymap[SDLK_7] = CEGUI::Key::Seven;
        keymap[SDLK_8] = CEGUI::Key::Eight;
        keymap[SDLK_9] = CEGUI::Key::Nine;
        keymap[SDLK_0] = CEGUI::Key::Zero;

        keymap[SDLK_q] = CEGUI::Key::Q;
        keymap[SDLK_w] = CEGUI::Key::W;
        keymap[SDLK_e] = CEGUI::Key::E;
        keymap[SDLK_r] = CEGUI::Key::R;
        keymap[SDLK_t] = CEGUI::Key::T;
        keymap[SDLK_y] = CEGUI::Key::Y;
        keymap[SDLK_u] = CEGUI::Key::U;
        keymap[SDLK_i] = CEGUI::Key::I;
        keymap[SDLK_o] = CEGUI::Key::O;
        keymap[SDLK_p] = CEGUI::Key::P;
        keymap[SDLK_a] = CEGUI::Key::A;
        keymap[SDLK_s] = CEGUI::Key::S;
        keymap[SDLK_d] = CEGUI::Key::D;
        keymap[SDLK_f] = CEGUI::Key::F;
        keymap[SDLK_g] = CEGUI::Key::G;
        keymap[SDLK_h] = CEGUI::Key::H;
        keymap[SDLK_j] = CEGUI::Key::J;
        keymap[SDLK_k] = CEGUI::Key::K;
        keymap[SDLK_l] = CEGUI::Key::L;
        keymap[SDLK_z] = CEGUI::Key::Z;
        keymap[SDLK_x] = CEGUI::Key::X;
        keymap[SDLK_c] = CEGUI::Key::C;
        keymap[SDLK_v] = CEGUI::Key::V;
        keymap[SDLK_b] = CEGUI::Key::B;
        keymap[SDLK_n] = CEGUI::Key::N;
        keymap[SDLK_m] = CEGUI::Key::M;

        keymap[SDLK_COMMA] = CEGUI::Key::Comma;
        keymap[SDLK_PERIOD] = CEGUI::Key::Period;
        keymap[SDLK_SLASH] = CEGUI::Key::Slash;
        keymap[SDLK_BACKSLASH] = CEGUI::Key::Backslash;
        keymap[SDLK_MINUS] = CEGUI::Key::Minus;
        keymap[SDLK_EQUALS] = CEGUI::Key::Equals;
        keymap[SDLK_SEMICOLON] = CEGUI::Key::Semicolon;
        keymap[SDLK_LEFTBRACKET] = CEGUI::Key::LeftBracket;
        keymap[SDLK_RIGHTBRACKET] = CEGUI::Key::RightBracket;
        keymap[SDLK_QUOTE] = CEGUI::Key::Apostrophe;
        keymap[SDLK_BACKQUOTE] = CEGUI::Key::Grave;

        keymap[SDLK_RETURN] = CEGUI::Key::Return;
        keymap[SDLK_SPACE] = CEGUI::Key::Space;
        keymap[SDLK_BACKSPACE] = CEGUI::Key::Backspace;
        keymap[SDLK_TAB] = CEGUI::Key::Tab;

        keymap[SDLK_ESCAPE] = CEGUI::Key::Escape;
        keymap[SDLK_PAUSE] = CEGUI::Key::Pause;
        keymap[SDLK_SYSREQ] = CEGUI::Key::SysRq;
        keymap[SDLK_POWER] = CEGUI::Key::Power;

        keymap[SDLK_NUMLOCKCLEAR] = CEGUI::Key::NumLock;
        keymap[SDLK_SCROLLLOCK] = CEGUI::Key::ScrollLock;

        keymap[SDLK_F1] = CEGUI::Key::F1;
        keymap[SDLK_F2] = CEGUI::Key::F2;
        keymap[SDLK_F3] = CEGUI::Key::F3;
        keymap[SDLK_F4] = CEGUI::Key::F4;
        keymap[SDLK_F5] = CEGUI::Key::F5;
        keymap[SDLK_F6] = CEGUI::Key::F6;
        keymap[SDLK_F7] = CEGUI::Key::F7;
        keymap[SDLK_F8] = CEGUI::Key::F8;
        keymap[SDLK_F9] = CEGUI::Key::F9;
        keymap[SDLK_F10] = CEGUI::Key::F10;
        keymap[SDLK_F11] = CEGUI::Key::F11;
        keymap[SDLK_F12] = CEGUI::Key::F12;
        keymap[SDLK_F13] = CEGUI::Key::F13;
        keymap[SDLK_F14] = CEGUI::Key::F14;
        keymap[SDLK_F15] = CEGUI::Key::F15;

        keymap[SDLK_LCTRL] = CEGUI::Key::LeftControl;
        keymap[SDLK_LALT] = CEGUI::Key::LeftAlt;
        keymap[SDLK_LSHIFT] = CEGUI::Key::LeftShift;
        keymap[SDLK_LGUI] = CEGUI::Key::LeftWindows;
        keymap[SDLK_RCTRL] = CEGUI::Key::RightControl;
        keymap[SDLK_RALT] = CEGUI::Key::RightAlt;
        keymap[SDLK_RSHIFT] = CEGUI::Key::RightShift;
        keymap[SDLK_RGUI] = CEGUI::Key::RightWindows;
        keymap[SDLK_MENU] = CEGUI::Key::AppMenu;

        keymap[SDLK_KP_0] = CEGUI::Key::Numpad0;
        keymap[SDLK_KP_1] = CEGUI::Key::Numpad1;
        keymap[SDLK_KP_2] = CEGUI::Key::Numpad2;
        keymap[SDLK_KP_3] = CEGUI::Key::Numpad3;
        keymap[SDLK_KP_4] = CEGUI::Key::Numpad4;
        keymap[SDLK_KP_5] = CEGUI::Key::Numpad5;
        keymap[SDLK_KP_6] = CEGUI::Key::Numpad6;
        keymap[SDLK_KP_7] = CEGUI::Key::Numpad7;
        keymap[SDLK_KP_8] = CEGUI::Key::Numpad8;
        keymap[SDLK_KP_9] = CEGUI::Key::Numpad9;
        keymap[SDLK_KP_PERIOD] = CEGUI::Key::Decimal;
        keymap[SDLK_KP_PLUS] = CEGUI::Key::Add;
        keymap[SDLK_KP_MINUS] = CEGUI::Key::Subtract;
        keymap[SDLK_KP_MULTIPLY] = CEGUI::Key::Multiply;
        keymap[SDLK_KP_DIVIDE] = CEGUI::Key::Divide;
        keymap[SDLK_KP_ENTER] = CEGUI::Key::NumpadEnter;

        keymap[SDLK_UP] = CEGUI::Key::ArrowUp;
        keymap[SDLK_LEFT] = CEGUI::Key::ArrowLeft;
        keymap[SDLK_RIGHT] = CEGUI::Key::ArrowRight;
        keymap[SDLK_DOWN] = CEGUI::Key::ArrowDown;

        keymap[SDLK_HOME] = CEGUI::Key::Home;
        keymap[SDLK_END] = CEGUI::Key::End;
        keymap[SDLK_PAGEUP] = CEGUI::Key::PageUp;
        keymap[SDLK_PAGEDOWN] = CEGUI::Key::PageDown;
        keymap[SDLK_INSERT] = CEGUI::Key::Insert;
        keymap[SDLK_DELETE] = CEGUI::Key::Delete;
    }

    void GUI::inject_mouseenter(SDL_Event* e)
    {

    }

    void GUI::inject_mouseleave(SDL_Event* e)
    {
        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        context.injectMouseLeaves();
    }

    void GUI::inject_mousemotion(SDL_Event* e)
    {
        int x, y;
        SDL_GetMouseState(&x, &y);
        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        context.injectMousePosition(static_cast<float>(x), static_cast<float>(y));
    }

    void GUI::inject_mousedown(SDL_Event* e)
    {
        unsigned char button = e->button.button;

        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        switch (button)
        {
        case SDL_BUTTON_LEFT:
            context.injectMouseButtonDown(CEGUI::LeftButton);
            break;
        case SDL_BUTTON_MIDDLE:
            context.injectMouseButtonDown(CEGUI::MiddleButton);
            break;
        case SDL_BUTTON_RIGHT:
            context.injectMouseButtonDown(CEGUI::RightButton);
            break;
        }
    }

    void GUI::inject_mouseup(SDL_Event* e)
    {
        unsigned char button = e->button.button;

        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        switch (button)
        {
        case SDL_BUTTON_LEFT:
            context.injectMouseButtonUp(CEGUI::LeftButton);
            break;
        case SDL_BUTTON_MIDDLE:
            context.injectMouseButtonUp(CEGUI::MiddleButton);
            break;
        case SDL_BUTTON_RIGHT:
            context.injectMouseButtonUp(CEGUI::RightButton);
            break;
        }
    }

    void GUI::inject_mousewheel(SDL_Event* e)
    {
        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        context.injectMouseWheelChange(static_cast<float>(e->wheel.y));
    }

    void GUI::inject_keydown(SDL_Event* e)
    {
        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        context.injectKeyDown(keymap[e->key.keysym.sym]);
    }

    void GUI::inject_keyup(SDL_Event* e)
    {
        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        context.injectKeyUp(keymap[e->key.keysym.sym]);
    }

    void GUI::inject_textinput(SDL_Event* e)
    {
        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        char* c = e->text.text;
        while (*c)
        {
            context.injectChar(*c);
            c = c + 1;
        }
    }

    void GUI::inject_videoresize(SDL_Event* e)
    {
        CEGUI::System::getSingleton().notifyDisplaySizeChanged(
            CEGUI::Sizef(static_cast<float>(e->window.data1), static_cast<float>(e->window.data2)));
    }

    void GUI::inject_frame_event()
    {
        CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse(static_cast<float>(Scheduler::get()->get_frame_delta()));
    }

}