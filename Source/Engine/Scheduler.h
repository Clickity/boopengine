#pragma once

#include <vector>
#include "GlobalResource.h"
#include <stdint.h>
#include <thread>
#include <mutex>

namespace Boop
{
	/// Handles frame updates, and time jumps.
	class Scheduler : public GlobalResource<Scheduler>
	{
	public:

		Scheduler();
		~Scheduler();
		
		/// Blocks the running thread (usually main) and runs the
		/// time update loops.
		/// @param frame_interval_ns The time in nanoseconds of each frame.
		void run_blocking(uint64_t frame_interval_ns);

		/// Stops the scheduler thread.
		void stop();
		
		/// @returns True if running.
		bool is_running();

		/// @returns the local epoch time.
		uint64_t get_localtime();

		/// @returns Frame interval in seconds.
		double get_frame_delta();

	private:

		std::thread::id main_thread_id;
		std::mutex sched_prot;
		uint64_t frame_interval;
		uint64_t frame_number;
		bool is_running_flag;
		bool exit_success;
	};

}