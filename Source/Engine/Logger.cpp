
#include "Logger.h"

#ifdef _WIN32
#include <Windows.h>
#else 
#include <iostream>
#endif

namespace Boop
{

	OutputBuffer::OutputBuffer(Logger* l)
		: logger(l)
	{

	}

	int OutputBuffer::sync()
	{
		std::string s = str();
		logger->pushString(s);
		str("");
		return 0;
	}

	Logger::Logger()
		: running(true)
		, outFile(0)
		, fWriteConsole(true)
		, fWriteFile(false)
		, fWriteNetwork(false)
	{
		thd = std::thread(&Logger::bgThread, this);
	}

	Logger::~Logger()
	{
		running = false;
		thd.join();

		for (auto it = bufferMap.begin(); it != bufferMap.end(); ++it)
		{
			delete it->second;
		}
	}

	void Logger::pushString(const std::string& str)
	{
		runner_mtx.lock();
		outputStrings.push_back(str);
		runner_mtx.unlock();
		sleeper.notify_all();
	}

	void Logger::enableConsoleLog()
	{
		fWriteConsole = true;
	}

	void Logger::disableConsoleLog()
	{
		fWriteConsole = false;
	}

	void Logger::enableFileLog(const char* filename, bool overwrite)
	{
		if (outFile)
		{
			outFile->close();
			delete outFile;
			outFile = 0;
		}

		std::ios_base::openmode mode = std::ofstream::out;
		if (overwrite)
		{
			mode |= std::ofstream::trunc;
		}
		else {
			mode |= std::ofstream::app;
		}
		outFile = new std::ofstream(filename, mode);
		if (outFile->is_open())
		{
			fWriteFile = true;
		}
		else {
			fWriteFile = false;
			delete outFile;
			outFile = 0;
		}

	}

	void Logger::disableFileLog()
	{
		if (outFile)
		{
			outFile->close();
			delete outFile;
			outFile = 0;
		}
		fWriteFile = false;
	}

	void Logger::enableNetworkLog(const char* filename)
	{
		fWriteNetwork = true;
	}

	void Logger::disableNetworkLog()
	{
		fWriteNetwork = false;
	}

	std::ostream& Logger::getStream()
	{
		std::thread::id id = std::this_thread::get_id();
		auto it = bufferMap.find(id);
		if (it == bufferMap.end())
		{
			bufferMap[id] = new Buffer(this);
		}
		return *bufferMap[id];
	}

	std::ostream& Logger::out()
	{
		return get()->getStream();
	}

	void Logger::bgThread()
	{
		while (running)
		{
			std::unique_lock<std::mutex> lck(runner_mtx);
			while (outputStrings.size() == 0 && running) sleeper.wait(lck);
			lck.unlock();
			while (outputStrings.size() > 0)
			{
				lck.lock();
				std::string str_here = outputStrings.front();
				outputStrings.pop_front();
				lck.unlock();

				if (fWriteConsole)
				{
#ifdef _WIN32
					OutputDebugStringA(str_here.c_str());
#else
					std::cout << str_here << std::flush;
#endif
				}

				if (fWriteFile)
				{
					(*outFile) << str_here << std::flush;
				}

				
			}
		}
	}

	Logger::Buffer::Buffer(Logger* l)
		: buffer(l)
		, std::ostream(&buffer)
	{

	}
}