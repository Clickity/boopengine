#include "PerfTimer.h"
#include <SDL.h>

namespace Boop
{

	PerfTimer::PerfTimer() :
		start_clock(0),
		end_clock(0)
	{
		start();
	}

	void PerfTimer::start()
	{
		start_clock = SDL_GetPerformanceCounter();
	}

	void PerfTimer::end()
	{
		end_clock = SDL_GetPerformanceCounter();
	}

	double PerfTimer::elapsed_seconds()
	{
		return static_cast<double>(end_clock - start_clock) / SDL_GetPerformanceFrequency();
	}

	double PerfTimer::elapsed_milliseconds()
	{
		return static_cast<double>(end_clock - start_clock) * 1000.0 / SDL_GetPerformanceFrequency();
	}

}