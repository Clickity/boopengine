#pragma once

#include "GlobalResource.h"
#include "GUISettings.h"
#include "Event.h"
#include "GUIRenderer.h"
#include <SDL.h>
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <unordered_map>

namespace Boop
{

	// GUI class using CEGUI. See CEGUI docs for more.
	class GUI : public GlobalResource<GUI>
	{
	public:
		GUI(const GUISettings& settings_i);
		~GUI();

	private:

		void deferred_init(void*);
		void init_key_map();

		void inject_mouseenter(SDL_Event* e);
		void inject_mouseleave(SDL_Event* e);
		void inject_mousemotion(SDL_Event* e);
		void inject_mousedown(SDL_Event* e);
		void inject_mouseup(SDL_Event* e);
		void inject_mousewheel(SDL_Event* e);
		void inject_keydown(SDL_Event* e);
		void inject_keyup(SDL_Event* e);
		void inject_textinput(SDL_Event* e);
		void inject_videoresize(SDL_Event* e);
		void inject_frame_event();

		DataEvent<SDL_Event>* mouseenter_event;
		DataEvent<SDL_Event>* mouseleave_event;
		DataEvent<SDL_Event>* mousemotion_event;
		DataEvent<SDL_Event>* mousedown_event;
		DataEvent<SDL_Event>* mouseup_event;
		DataEvent<SDL_Event>* mousewheel_event;
		DataEvent<SDL_Event>* keydown_event;
		DataEvent<SDL_Event>* keyup_event;
		DataEvent<SDL_Event>* textinput_event;
		DataEvent<SDL_Event>* videoresize_event;
		Event* frame_event;
		GUISettings settings;
		std::map<SDL_Keycode, CEGUI::Key::Scan> keymap;
		CEGUI::OpenGL3Renderer* guirenderer;
		GUIRenderer renderer;
	};

}