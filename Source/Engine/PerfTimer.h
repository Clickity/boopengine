#pragma once

#include <stdint.h>

namespace Boop
{
	/// Use to time code sections.
	class PerfTimer
	{
	public:

		PerfTimer();
		void start();
		void end();
		double elapsed_seconds();
		double elapsed_milliseconds();

	private:
		uint64_t start_clock;
		uint64_t end_clock;
	};

}