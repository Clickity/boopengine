#pragma once

#include <stdint.h>
#include <limits.h>

#include <chrono>
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <Logger.h>

// Various utilitys

#define PI 3.14159265358979323846f
#define PI_2 1.57079632679f
#define SQRT_2 1.41421356237f

namespace Boop
{

	class Perf
	{
	public:
		Perf();
		void start();
		void end();
		double getTime();
		double getTimeMs();
		void print(const char* tag = "");
		void printms(const char* tag = "");
	private:
		uint64_t startn;
		uint64_t endn;
	};


	/// @returns The current epoch time in nanoseconds.
	uint64_t now();

	/// @returns A random number within and including the given range.
	int random(int min, int max);

	/// @returns A random number within and including the given range.
	float random(float min, float max);

	/// @returns A simple 64 bit unique id.
	uint64_t makeuid();

	/// Fast cosine using lookup table.
	float fastcos(float n);

	/// Fast sine using lookup table.
	float fastsin(float n);

	/// Fast atan2 using lookup table.
	float fastatan2(float y, float x);

	/// Linear interpolation
	float linearInterpolation(float x1, float x2, float r);

	/// Cosing interpolation
	float cosineInterpolation(float x1, float x2, float r);

	/// Builds fast sine lookup table. Called in `Application` constructor.
	void buildFastCosineTable();

	/// 
	void FloatToInt(int* int_pointer, float f);

}