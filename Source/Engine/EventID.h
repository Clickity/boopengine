#pragma once

#include <SDL.h>

namespace Boop
{
	// The channels used by the engine
	enum Channel
	{
		SYSTEM,
		SDL,
		NETWORK,
	};

	// System events called by the engine
	enum SystemEventID
	{
		FRAME_UPDATE, // Called by the scheduler every frame
		QUIT          // Called when the window is asked to close
	};

}