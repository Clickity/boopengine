#pragma once

#include "GlobalResource.h"
#include <unordered_map>
#include <vector>
#include <Pool.h>
#include <PooledList.h>
#include <functional>
#include <mutex>
#include "ThreadPool.h"

namespace Boop
{

	enum Thread_Constants
	{
		/// This value is useful if there is no thread pool. It will just run on the dispatched thread.
		NO_THREAD = -1,

		/// This value means the dispatcher will run the event on the dispatched thread.
		THIS_THREAD = -2,

		/// Any thread will schedule the thread to run on the least utilized thread.
		ANY_THREAD = -3
	};

	class EventDispatcher : public GlobalResource<EventDispatcher>
	{
	public:
		typedef std::function<void(void*)> EventFunction;

		struct EventInstance
		{
			EventFunction func;
			int thread_num;
			uint64_t uid;
			bool finished_flag;
			bool is_repeating;
			bool can_duplicate;
		};

		/// @param num_events The max number of events that can be added.
		EventDispatcher(int num_events);

		/// Set the thread pool to be used.
		void set_thread_pool(ThreadPool* thread_pool);

		/// @returns The current thread pool.
		ThreadPool* get_thread_pool();

		/// Dispatches an event to all added listeners.
		/// @param event_id The event to dispatch.
		/// @param channel The channel to dispatch on.
		/// @param data Optional data to send to each listening event.
		void dispatch(uint16_t event_id, uint8_t channel, std::shared_ptr<void> data);

		/// Adds an event to listen.
		/// @param event_id The event to listen for.
		/// @param channel The channel to listen on.
		/// @param uid A unique identifier used for removing the event.
		/// @param func The callback function.
		/// @param thread_num The thread to run on, optionally `Thread_Constants` can be used.
		/// @param is_repeating False to remove the thread after it runs once.
		/// @param can_duplicate False if the event can only have one instance running at a time.
		void add(uint16_t event_id, uint8_t channel, uint64_t uid, EventFunction func, int thread_num, bool is_repeating, bool can_duplicate);

		/// Removes the event with the given unique identifier.
		bool remove(uint16_t event_id, uint8_t channel, uint64_t uid);

		/// Bypasses add the event to the queue, and just runs the callback now on the specified thread.
		/// @param thread_num The thread to run on, optionally `Thread_Constants` can be used.
	    /// @param func The callback function.
		/// @param data Shared pointer to data to send to the function. NULL is valid if no data is necessary.
		/// @param finished_flag Pointer to a flag that is set to true when the function has finishe execution.
		void run_this(int thread_num, std::function<void(void*)> func, std::shared_ptr<void> data = 0, bool* finished_flag = 0);


	private:

		uint32_t getCombinedIndex(uint16_t event_id, uint8_t channel);

		std::unordered_map<uint32_t, uint32_t> binding_map;
		std::vector<PooledList<EventInstance>> pool_ref;
		DynamicPool<PooledList<EventInstance>::Node> event_pool;
		std::recursive_mutex event_map_prot;
		ThreadPool* thread_pool;
		int last_event_id;
	};

}