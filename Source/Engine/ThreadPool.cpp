#include "ThreadPool.h"
#include <thread>

namespace Boop
{

	ThreadPool::ThreadPool(uint8_t num_threads)
	{
		for (uint8_t i = 0; i < num_threads; ++i)
		{
			workers.push_back(new WorkerThread());
		}
	}

	ThreadPool::~ThreadPool()
	{
		for (uint8_t i = 0; i < workers.size(); ++i)
		{
			delete workers[i];
		}
	}

	WorkerThread& ThreadPool::get_thread(uint8_t thread_num)
	{
		return *workers[thread_num];
	}


	int ThreadPool::get_num_threads()
	{
		return static_cast<int>(workers.size());
	}

	int ThreadPool::get_this_thread()
	{
		std::thread::id this_thread_id = std::this_thread::get_id();
		for (int i = 0; i < workers.size(); ++i)
		{
			if (workers[i]->get_thread_id() == this_thread_id)
			{
				return i;
			}
		}
		return -1;
	}

}