
#include "FileTransfer.h"
#include "Logger.h"
#include "RakAssert.h"
#include <filesystem>
#include "Network.h"

namespace Boop
{

	bool FileDownloadProgress::OnFile(OnFileStruct* onFileStruct)
	{
		printf("OnFile: %i. (100%%) %i/%i %s %ib / %ib\n",
			onFileStruct->setID,
			onFileStruct->fileIndex + 1,
			onFileStruct->numberOfFilesInThisSet,
			onFileStruct->fileName,
			onFileStruct->byteLengthOfThisFile,
			onFileStruct->byteLengthOfThisSet);

		std::filesystem::path path = Network::get()->getDownloadLocation();
		path.append(onFileStruct->fileName);
		FILE* fp = fopen(path.string().c_str(), "wb");
		fwrite(onFileStruct->fileData, onFileStruct->byteLengthOfThisFile, 1, fp);
		fclose(fp);

		// Make sure it worked
		//unsigned int hash1 = SuperFastHashFile(file.C_String());
		//if (RakNet::BitStream::DoEndianSwap())
		//	RakNet::BitStream::ReverseBytesInPlace((unsigned char*)&hash1, sizeof(hash1));
		//unsigned int hash2 = SuperFastHashFile(fileCopy.C_String());
		//if (RakNet::BitStream::DoEndianSwap())
		//	RakNet::BitStream::ReverseBytesInPlace((unsigned char*)&hash2, sizeof(hash2));
		//RakAssert(hash1 == hash2);

		// Return true to have RakNet delete the memory allocated to hold this file.
		// False if you hold onto the memory, and plan to delete it yourself later
		return true;
	}

	void FileDownloadProgress::OnFileProgress(FileProgressStruct* fps)
	{
		Logger::out() << "OnFileProgress: " << fps->onFileStruct->setID
			<< " partCount=" << fps->partCount
			<< " partTotal=" << fps->partTotal
			<< " (" << (int)(100.0 * (double)fps->onFileStruct->bytesDownloadedForThisFile / (double)fps->onFileStruct->byteLengthOfThisFile) << "%) "
			<< fps->onFileStruct->fileIndex + 1 << "/" << fps->onFileStruct->numberOfFilesInThisSet
			<< " " << fps->onFileStruct->fileName
			<< " " << fps->onFileStruct->bytesDownloadedForThisFile << "/" << fps->onFileStruct->byteLengthOfThisFile
			<< " " << fps->onFileStruct->bytesDownloadedForThisSet << "/" << fps->onFileStruct->byteLengthOfThisSet
			<< "total" << std::endl;
	}

	bool FileDownloadProgress::OnDownloadComplete(DownloadCompleteStruct* dcs)
	{
		Logger::out() << "Download complete." << std::endl;

		RakNet::BitStream bs;
		bs.Write<uint8_t>(Network::Messages::ID_FILE_DOWNLOAD_COMPLETE_NOTIFY);

		Network::get()->getPeer()->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, Network::FILE_ORDER_CHANNEL, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

		// Returning false automatically deallocates the automatically allocated handler that was created by DirectoryDeltaTransfer
		return false;
	}

	void FileDownloadProgress::OnDereference()
	{
	}


	void FileUploadProgress::OnFilePush(const char* fileName, unsigned int fileLengthBytes, unsigned int offset, unsigned int bytesBeingSent, bool done, RakNet::SystemAddress targetSystem, unsigned short setID)
	{
		Logger::out() << "Sending " << fileName << " bytes=" << bytesBeingSent << " offset=" << offset << std::endl;
	}

	void FileUploadProgress::OnFilePushesComplete(RakNet::SystemAddress systemAddress, unsigned short setID)
	{
		char str[32];
		systemAddress.ToString(true, (char*)str);
		Logger::out() << "File pushes complete to " << str << std::endl;

		std::vector<Network::TransferRequest>& requests = Network::get()->getRequests();
		for (auto it = requests.begin(); it != requests.end(); ++it)
		{
			if (it->setID == setID)
			{
				*it->status = Network::COMPLETED;
				requests.erase(it);
				break;
			}
		}
	}

	void FileUploadProgress::OnSendAborted(RakNet::SystemAddress systemAddress)
	{
		char str[32];
		systemAddress.ToString(true, (char*)str);
		Logger::out() << "Send aborted to " << str << std::endl;

		std::vector<Network::TransferRequest>& requests = Network::get()->getRequests();
		for (auto it = requests.begin(); it != requests.end(); ++it)
		{
			if (it->systemAddress == systemAddress)
			{
				*it->status = Network::FAILED;
				requests.erase(it);
			}
		}
	}

}