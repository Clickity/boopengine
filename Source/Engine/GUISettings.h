#pragma once

#include <string>

namespace Boop
{
	
	class GUISettings
	{
	public:
		/// The resource directory for CEGUI
		std::string resource_directory;

		/// The thread to run CEGUI on.
		int thread_num;
	};

}