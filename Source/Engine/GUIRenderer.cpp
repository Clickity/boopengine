
#include "GUIRenderer.h"
#include <CEGUI/CEGUI.h>
#include <GL/glew.h>
#include "Logger.h"


namespace Boop
{

	void GUIRenderer::draw()
	{
		glDisable(GL_DEPTH_TEST);
		CEGUI::System::getSingleton().renderAllGUIContexts();
	}

}