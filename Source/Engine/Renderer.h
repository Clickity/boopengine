#pragma once

namespace Boop
{
	/// Interface for renderer's using `RenderManager`
	class Renderer
	{
	public:
		Renderer();

		virtual void draw() = 0;

		void show();

		void hide();

		bool is_hidden();

	private:

		bool is_hidden_f;
	};

}