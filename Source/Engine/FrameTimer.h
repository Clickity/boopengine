#pragma once

#include <stdint.h>

namespace Boop
{

	// Times since the last time it was called.
	class FrameTimer
	{
	public:
		FrameTimer();
		~FrameTimer();

		// Restarts the timer.
		// @returns the time elapsed since `elapsed` was called.
		double elapsed();

		// @returns the last time elapsed without restarting the timer.
		double last_elapsed();

	private:

		uint64_t last_cycle;
		double ret;
	};

}