
#include <GL/glew.h>
#include <WinSock2.h>
#include <stdio.h>
#include <assert.h>
#include <Application.h>
#include <GUI.h>
#include <Logger.h>
#include <PerfTimer.h>
#include <ThreadPool.h>
#include <EventDispatcher.h>
#include <Scheduler.h>
#include <Event.h>
#include <EventID.h>
#include <RenderManager.h>
#include <SceneManager.h>
#include <ShaderManager.h>
#include "Network.h"

#ifdef CLIENT
#include <GameScene.h>
#elif SERVER
#include <ServerScene.h>
#endif


void debug_frame_update()
{
	Boop::Logger::out() << "Frame time: " << Boop::Scheduler::get()->get_localtime() << std::endl;
}

void post_resource_init(void*)
{
#ifdef CLIENT
	assert(Boop::Application::is_initialized());
	assert(Boop::GUI::is_initialized());

	CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("Artificial.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("Generic.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("GlossySerpentFHD.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("VanillaSkin.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("VanillaCommonDialogs.scheme");
	CEGUI::SchemeManager::getSingleton().createFromFile("WindowsLook.scheme");

	//CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-10");
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("Vanilla-Images/MouseArrow");
	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultTooltipType("GlossySerpentFHD/Tooltip");

	CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
	CEGUI::Window*  root = wmgr.createWindow("DefaultWindow", "root");
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(root);

	Boop::SceneManager::create()->next_scene<GameScene>();
#elif SERVER
	Boop::SceneManager::create()->next_scene<ServerScene>();
#endif
}

int main(int argc, char** argv)
{
	Boop::Logger::create();

	Boop::Logger::out() << "Program Start" << std::endl;

	Boop::ThreadPool thread_pool(3);
	Boop::EventDispatcher::create(1000)->set_thread_pool(&thread_pool);

#ifdef CLIENT
	Boop::ApplicationSettings app_settings;
	app_settings.application_name = "Artificial";
	app_settings.is_default_resolution = false;
	app_settings.is_fullscreen = false;
	app_settings.is_maximized = true;
	app_settings.is_resizable = false;
	app_settings.window_offset_x = 150;
	app_settings.window_offset_y = 150;
	app_settings.window_width = 500;
	app_settings.window_height = 500;
	app_settings.thread_num = 0;
	Boop::Application::create(app_settings);

	Boop::RenderManager::create(1);

	Boop::GUISettings gui_settings;
	gui_settings.resource_directory = "Assets/";
	gui_settings.thread_num = 0;
	Boop::GUI::create(gui_settings);

	Boop::ShaderManager::create();
#endif

	Boop::Network* network = Boop::Network::create(1);
	
#ifdef SERVER
	network->startServer(10);
#endif
	network->enableFileTransfer();

	Boop::EventDispatcher::get()->run_this(0, &post_resource_init);
	Boop::Scheduler::create()->run_blocking(16666667, 50000);

	Boop::Logger::out() << "Program End" << std::endl;

	return 0;
}

