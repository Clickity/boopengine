Boop Game Engine
A game engine optimized for CPU performance with easy multi-threading.

I wrote thie engine so I had a framework for both 2D and 3D games. I wanted it to be really easy to schedule
tasks in other threads because I've seen a lot of games not utilizing CPU cores effectively. I'm a big
fan of pool and static allocators for performance optimization. I really enjoy using this system, I hope
you find something useful here for your project.

Features:

* Event dispatcher that you can plug any system into
* Custom containers
* Background buffered logging
* Basic application setup and scene manager
* Basic render manager
* Basic tuexture loader
* Shader manager
* Basic client/server networking framework

Basic Usage:

Most classes are derived from `GlobalResource`. You'll have to pick which you want to use and create the
objects. There's an example on how to start them in `Entry-sample.cpp`, but basically you just call the
`create(...)` method with theh initialization paramaters.

The `EventDispatcher` allows you to easily hook in third-party libraries and schedule tasks to run
on the desired thread. However, you should only use the `EventDispatcher` directly if your dispatching an
event. To sign up for an you use `Event` and `DataEvent`. These classes handle creation, destruction,
and event scheduling for you.

Here's an example:

```
#include <Event.h>
#include <EventID.h

class HandleKeyboard
{
public:
    HandleKeyboard()
        : mouseMoveEvent(SDL_MouseMotionEvent, Boop::Channel::SDL, Boop::THIS_THREAD, BIND_BOOPDATAEVENT(&onMouseMove, this))
    {
        mouseMoveEvent.poll();
    }
private:
    void onMouseMove(SDL_Event* e)
    {
        // Move moved
    }
    
    Boop::DataEvent<SDL_Event> mouseMoveEvent;
}
```

This code creates a class that will poll for the mouse move event.

There are three types of event sign ups.

* `poll()` will fire the event every time dispatch is called.
* `repeat()` will fire if dispatch is called and the last call has already completed.
* `run()` will fire the when dispatch is called, but it only runs once per run call.

The Event ID you sign up for depends on the channel you want to listen on. Two channels can have
the same event ID because the dispatch function maps events to an Event ID AND a Channel ID.

Some built-in Channels are:

- Boop::Channel::SYSTEM Used by the scheduler for frame update events and exit events.
- Boop::Channel::SDL Used to signup for an SDL event. This works using the `type` field of SDL_Event.
- Boop::Channel::Network Used to sign up for messages.


Dependencies:

The dependencies I used will come with the build, but for reference I typically used:

* SDL2         https://www.libsdl.org/
* SDL2_image   https://www.libsdl.org/projects/SDL_image/
* RakNet       http://www.raknet.net/
* and CEGUI    http://cegui.org.uk/
 
 
Logging:

The Logger is the first thing you want to start, because some of the other classes use it.

For example:

```
Boop::Logger::create();
Boop::Logger::out() << "Program Start" << std::endl;
```

Printing to console and logging to files can be slow, so the logger background thread will pick
up the buffered data when there is data to buffer. This prevents blocking file I/O from ruining
the performance of your main threads.

Building and Running:

Build with requires C++17
After linking with the dependencies I usually add this like to the debugger environment so I don't have
to move all the DLLs

`PATH=%PATH%;$(ProjectDir)\Dependencies\cegui-0.8.7\build\bin;$(ProjectDir)\Dependencies\cegui-0.8.7\dependencies\bin;$(ProjectDir)\Dependencies\SDL2_image-2.0.5\lib\x64;$(ProjectDir)/Dependencies/SDL2-2.0.10/lib/x64`


Liscenses:

The dependencies have their own licenses, but this code just open source MIT.


