#version 330 core

layout(location = 0) in vec2 vertex;
layout(location = 1) in vec2 texcoord;

out vec2 texcoord_vs;

uniform vec3 position;
uniform float rotation;
uniform vec2 size;
uniform vec2 scale;

vec2 rotate(vec2 v, float a) 
{
	float s = sin(a);
	float c = cos(a);
	mat2 m = mat2(c, -s, s, c);
	return m * v;
}

void main()
{
	vec2 res = vertex * size * 0.5f;
	res = rotate(res, rotation);
	gl_Position = vec4(scale * (position.xy + res) - vec2(1.f, 1.f), position.z, 1.0f);
	texcoord_vs = texcoord;
}
